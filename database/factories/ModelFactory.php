<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->username,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Userinfo::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'id_number' => $faker->randomNumber($nbDigits = 8),
        'mobile' => $faker->phoneNumber,
        'accept_sms' => False,
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'contact' => $faker->name,
        'contact_title' => "manager",
        'contact_phone' => $faker->phoneNumber,
        'contact_email' => $faker->email,
    ];
});

$factory->define(App\CarCompany::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'contact' => $faker->name,
        'contact_title' => "manager",
        'contact_phone' => $faker->phoneNumber,
        'contact_email' => $faker->email,
    ];
});

$factory->define(App\Driver::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'id_number' => $faker->randomNumber($nbDigits = 8),
        'mobile' => $faker->phoneNumber,
        'accept_sms' => False,
    ];
});

$factory->define(App\Car::class, function (Faker\Generator $faker) {
    return [
        'car_type_id' => App\CarType::first()->id,
        'lpn' => $faker->randomNumber($nbDigits = 8),
        'model' => $faker->name,
        'birth_year' => $faker->year($max = 'now'),
    ];
});