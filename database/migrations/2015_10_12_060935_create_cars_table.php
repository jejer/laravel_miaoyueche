<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_company_id')->unsigned();
            $table->integer('car_type_id')->unsigned();
            $table->string('lpn')->unique();
            $table->string('model');
            $table->integer('birth_year');
            $table->string('pic_license')->nullable();
            $table->string('pic_insurance')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('car_company_id')
                  ->references('id')
                  ->on('car_companies')
                  ->onDelete('cascade');
            $table->foreign('car_type_id')
                  ->references('id')
                  ->on('car_types')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cars');
    }
}
