<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceStrategiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_strategies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_type_id')->unsigned();
            $table->foreign('order_type_id')
                  ->references('id')
                  ->on('order_types')
                  ->onDelete('cascade');
            $table->integer('car_type_id')->unsigned();
            $table->foreign('car_type_id')
                  ->references('id')
                  ->on('car_types')
                  ->onDelete('cascade');

            $table->string('name');
            $table->double('base_price', 9, 2);
            $table->double('overtime_price', 9, 2);
            $table->double('overmile_price', 9, 2);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_strategies');
    }
}
