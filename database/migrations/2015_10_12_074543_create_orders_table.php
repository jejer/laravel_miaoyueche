<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            // parent order
            $table->integer('parent_order_id')->unsigned()->nullable();
            $table->foreign('parent_order_id')
                  ->references('id')
                  ->on('orders')
                  ->onDelete('cascade');

            // owner
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')
                  ->references('id')
                  ->on('customers')
                  ->onDelete('cascade');

            // order type
            $table->integer('order_type_id')->unsigned();
            $table->foreign('order_type_id')
                  ->references('id')
                  ->on('order_types')
                  ->onDelete('cascade');
            $table->integer('order_state_id')->unsigned();
            $table->foreign('order_state_id')
                  ->references('id')
                  ->on('order_states')
                  ->onDelete('cascade');
            $table->integer('car_type_id')->unsigned();
            $table->foreign('car_type_id')
                  ->references('id')
                  ->on('car_types')
                  ->onDelete('cascade');

            // contact info
            $table->string('department');
            $table->string('contact');
            $table->string('mobile');

            // order info
            $table->timestamp('time_use');
            $table->string('location_start')->nullable();
            $table->string('location_end')->nullable();
            $table->string('flight_no')->nullable();
            $table->boolean('is_pickup')->default(False);

            // car assignment
            $table->integer('car_company_id')->unsigned()->nullable();
            $table->foreign('car_company_id')
                  ->references('id')
                  ->on('car_companies')
                  ->onDelete('cascade');
            $table->integer('car_id')->unsigned()->nullable();
            $table->foreign('car_id')
                  ->references('id')
                  ->on('cars')
                  ->onDelete('cascade');
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')
                  ->references('id')
                  ->on('drivers')
                  ->onDelete('cascade');

            // order data
            $table->timestamp('time_start')->nullable();
            $table->timestamp('time_end')->nullable();
            $table->double('mile_start', 9, 2)->nullable();
            $table->double('mile_end', 9, 2)->nullable();

            // signature
            $table->string('sign_pickup')->unique()->nullable();
            $table->string('sign_final')->unique()->nullable();

            // price
            $table->double('price_final', 9, 2)->nullable();
            $table->timestamp('time_final')->nullable();

            // misc
            $table->boolean('is_complain')->default(False);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
