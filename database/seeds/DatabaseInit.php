<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);


        // 权限
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();
        DB::table('role_user')->delete();

        DB::table('roles')->insert(['name' => 'customer']);
        DB::table('roles')->insert(['name' => 'customer_service']);
        DB::table('roles')->insert(['name' => 'car_company']);
        DB::table('roles')->insert(['name' => 'driver']);

        // 平台客服
        DB::table('users')->delete();
        DB::table('userinfos')->delete();

        $admin = App\User::create(['name' => 'admin', 
             'email' => 'admin@miaoyueche.com', 'password' => bcrypt('qyltaa1111')]);
        $admin->assignRole('customer_service');
        App\Userinfo::create(['user_id' => $admin->id, 'name' => '管理员', 
             'mobile' => '']);

        // 车型
        DB::table('car_types')->delete();
        App\CarType::create(['type' => 'eco', 'name' => '经济型']);
        App\CarType::create(['type' => 'com', 'name' => '舒适型']);
        App\CarType::create(['type' => 'bus_7', 'name' => '商务7座']);
        App\CarType::create(['type' => 'suv', 'name' => 'SUV']);
        App\CarType::create(['type' => 'lux', 'name' => '豪华型']);
        App\CarType::create(['type' => 'cmb_14_55', 'name' => '14-55座中巴']);

        // 订单类型
        DB::table('order_types')->delete();
        App\OrderType::create(['type' => 'airport', 'name' => '接送机']);
        App\OrderType::create(['type' => 'halfday', 'name' => '半日租代驾']);
        App\OrderType::create(['type' => 'halfday_self', 'name' => '半日租自驾']);
        App\OrderType::create(['type' => 'fullday', 'name' => '全日租代驾']);
        App\OrderType::create(['type' => 'fullday_self', 'name' => '全日租自驾']);

        // 订单状态
        DB::table('order_states')->delete();
        App\OrderState::create(['state' => 'finished', 'name' => '完成']);
        App\OrderState::create(['state' => 'deleted', 'name' => '被删除']);
        App\OrderState::create(['state' => 'w_cs_asn', 'name' => '等待客服配车']);
        App\OrderState::create(['state' => 'w_cus_cfm_car', 'name' => '等待用户确认用车']);
        App\OrderState::create(['state' => 'w_drv_fin_info', 'name' => '等待司机输入结束信息']);
        App\OrderState::create(['state' => 'w_cus_cfm_fin', 'name' => '等待用户最终确认']);
        App\OrderState::create(['state' => 'w_drv_start_info', 'name' => '等待司机输入起始信息']);
        App\OrderState::create(['state' => 'w_cus_cfm_pickup', 'name' => '等待用户确认提车']);

        // 价格策略
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'接送机 经济', 'base_price' => 160.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'接送机 舒适', 'base_price' => 180.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'接送机 商务7座', 'base_price' => 200.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'接送机 SUV', 'base_price' => 200.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'接送机 豪华', 'base_price' => 320.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'接送机 14-55座中巴', 'base_price' => 360.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'半日租代驾 经济', 'base_price' => 260.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'半日租代驾 舒适', 'base_price' => 280.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'半日租代驾 商务7座', 'base_price' => 300.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'半日租代驾 SUV', 'base_price' => 320.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'半日租代驾 豪华', 'base_price' => 450.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'半日租代驾 14-55座中巴', 'base_price' => 550.00, 'overtime_price' => 20.00, 'overmile_price' => 5.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'全日租代驾 经济', 'base_price' => 350.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'全日租代驾 舒适', 'base_price' => 460.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'全日租代驾 商务7座', 'base_price' => 480.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'全日租代驾 SUV', 'base_price' => 500.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'全日租代驾 豪华', 'base_price' => 600.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'全日租代驾 14-55座中巴', 'base_price' => 850.00, 'overtime_price' => 20.00, 'overmile_price' => 5.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'全日租自驾 经济', 'base_price' => 150.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'全日租自驾 舒适', 'base_price' => 260.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'全日租自驾 商务7座', 'base_price' => 280.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'全日租自驾 SUV', 'base_price' => 300.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'全日租自驾 豪华', 'base_price' => 400.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);

        Model::reguard();
    }
}
