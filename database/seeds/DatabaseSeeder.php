<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $COUNT = 2;


        // 权限
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();
        DB::table('role_user')->delete();

        DB::table('roles')->insert(['name' => 'customer']);
        DB::table('roles')->insert(['name' => 'customer_service']);
        DB::table('roles')->insert(['name' => 'car_company']);
        DB::table('roles')->insert(['name' => 'driver']);

        // 平台客服
        DB::table('users')->delete();
        DB::table('userinfos')->delete();

        $cs1 = App\User::create(['name' => 'cs1',
            'email' => 'cs1@cs1.com', 'password' => bcrypt('cs1')]);
        $cs1->assignRole('customer_service');
        $cs1->userinfo()->create(['name' => '客服cs1', 'mobile' => '18605819527']);

        $cs2 = App\User::create(['name' => 'admin',
            'email' => 'admin@miaoyueche.com', 'password' => bcrypt('qyltaa1111')]);
        $cs2->assignRole('customer_service');
        $cs2->userinfo()->create(['name' => '管理员', 'mobile' => '15376426198']);

        factory('App\User', $COUNT)->create()->each(function($u) {
            $u->assignRole('customer_service');
            factory('App\Userinfo')->create(['user_id' => $u->id]);
        });

        // 客户
        DB::table('customers')->delete();

        $cu1 = App\User::create(['name' => 'cu1',
                                 'email' => 'cu1@cu1.com', 'password' => bcrypt('cu1')]);
        $cu1->assignRole('customer');
        $customer1 = $cu1->customer()->create(['name' => '客户1 公司',
            'contact' => '客户1 公司 联系人', 'contact_title' => '秘书',
            'contact_phone' => '0532-019182', 'contact_email' => 'cu1@cu1.com']);

        $cu2 = App\User::create(['name' => 'cu2',
             'email' => 'cu2@cu2.com', 'password' => bcrypt('cu2')]);
        $cu2->assignRole('customer');
        $customer2 = $cu2->customer()->create(['name' => '客户2 公司',
            'contact' => '客户2 公司 联系人', 'contact_title' => '秘书',
            'contact_phone' => '0532-019182', 'contact_email' => 'cu2@cu2.com']);

        factory('App\User', $COUNT)->create()->each(function($u) {
            $u->assignRole('customer');
            factory('App\Customer')->create(['user_id' => $u->id]);
        });

        // 租车公司
        DB::table('car_companies')->delete();

        $cc1 = App\User::create(['name' => 'cc1',
             'email' => 'cc1@cc1.com', 'password' => bcrypt('cc1')]);
        $cc1->assignRole('car_company');
        $car_company1 = $cc1->car_company()->create(['name' => '租车公司1 公司',
            'contact' => '租车公司1 公司 联系人', 'contact_title' => '秘书',
            'contact_phone' => '0532-019182', 'contact_email' => 'cc1@cc1.com']);

        $cc2 = App\User::create(['name' => 'cc2',
             'email' => 'cc2@cc2.com', 'password' => bcrypt('cc2')]);
        $cc2->assignRole('car_company');
        $car_company2 = $cc2->car_company()->create(['name' => '租车公司2 公司',
            'contact' => '租车公司2 公司 联系人', 'contact_title' => '秘书',
            'contact_phone' => '0532-019182', 'contact_email' => 'cc2@cc2.com']);

        factory('App\User', $COUNT)->create()->each(function($u) {
            $u->assignRole('car_company');
            factory('App\CarCompany')->create(['user_id' => $u->id]);
        });

        // 司机
        $dr1 = App\User::create(['name' => 'driver1', 'wx_openid' => 'oEAffvwsrzrgrcTrktcIpY6TgWmg',
             'email' => 'driver1@driver1.com', 'password' => bcrypt('driver1')]);
        $dr1->assignRole('driver');
        $driver1 = $dr1->driver()->create(['name' => '司机1',
                            'id_number' => '371311111111111111', 'mobile' => '18605819527']);

        $dr2 = App\User::create(['name' => 'driver2',//  'wx_openid' => 'ojqAWw7eWXE_vpy0dR2cZdWbs4Vo',
             'email' => 'driver2@driver2.com', 'password' => bcrypt('driver2')]);
        $dr2->assignRole('driver');
        $driver2 = $dr2->driver()->create(['name' => '司机2',
                            'id_number' => '123', 'mobile' => '15376426198']);

        factory('App\User', $COUNT)->create()->each(function($u) {
            $u->assignRole('driver');
            factory('App\Driver')->create(['user_id' => $u->id]);
        });


        // 车型
        DB::table('car_types')->delete();
        App\CarType::create(['type' => 'eco', 'name' => '经济型']);
        App\CarType::create(['type' => 'com', 'name' => '舒适型']);
        App\CarType::create(['type' => 'bus_7', 'name' => '商务7座']);
        App\CarType::create(['type' => 'suv', 'name' => 'SUV']);
        App\CarType::create(['type' => 'lux', 'name' => '豪华型']);
        App\CarType::create(['type' => 'cmb_14_55', 'name' => '14-55座中巴']);

        // 车辆
        factory('App\Car', $COUNT)->create(['car_company_id' => $car_company1->id]);
        factory('App\Car', $COUNT)->create(['car_company_id' => $car_company2->id]);

        // 订单类型
        DB::table('order_types')->delete();
        App\OrderType::create(['type' => 'airport', 'name' => '接送机']);
        App\OrderType::create(['type' => 'halfday', 'name' => '半日租代驾']);
        App\OrderType::create(['type' => 'halfday_self', 'name' => '半日租自驾']);
        App\OrderType::create(['type' => 'fullday', 'name' => '全日租代驾']);
        App\OrderType::create(['type' => 'fullday_self', 'name' => '全日租自驾']);

        // 订单状态
        DB::table('order_states')->delete();
        App\OrderState::create(['state' => 'finished', 'name' => '完成']);
        App\OrderState::create(['state' => 'deleted', 'name' => '被删除']);
        App\OrderState::create(['state' => 'w_cs_asn', 'name' => '等待客服配车']);
        App\OrderState::create(['state' => 'w_cus_cfm_car', 'name' => '等待用户确认用车']);
        App\OrderState::create(['state' => 'w_drv_fin_info', 'name' => '等待司机输入结束信息']);
        App\OrderState::create(['state' => 'w_cus_cfm_fin', 'name' => '等待用户最终确认']);
        App\OrderState::create(['state' => 'w_drv_start_info', 'name' => '等待司机输入起始信息']);
        App\OrderState::create(['state' => 'w_cus_cfm_pickup', 'name' => '等待用户确认提车']);

        // 订单
        DB::table('orders')->delete();
        $order1 = App\Order::create(['customer_id' => $customer1->id,
                                     'order_type_id' => 'airport',
                                     'order_state_id' => 'w_cs_asn',
                                     'car_type_id' => 'eco',
                                     'department' => 'DEPARTMENT',
                                     'contact' => 'CONTACT',
                                     'mobile' => '18605819527',
                                     'time_use' => Carbon::now()]);
        $order1->order_charges()->create(['name' => '过路', 'price' => 25]);
        $order1->order_histories()->create(['operator' => '客户', 'info' => '您提交了订单，请等待客服配车']);


        App\Order::create(['customer_id' => $customer1->id,
                           'order_type_id' => 'airport',
                           'order_state_id' => 'w_cs_asn',
                           'car_type_id' => 'suv',
                           'department' => 'DEPARTMENT2',
                           'contact' => 'CONTACT2',
                           'mobile' => '18605819527',
                           'time_use' => Carbon::now()]);

        $parent = App\Order::create(['customer_id' => $customer1->id,
                                     'order_type_id' => 'fullday',
                                     'order_state_id' => 'w_drv_start_info',
                                     'car_type_id' => 'suv',
                                     'department' => 'DEPARTMENT3',
                                     'contact' => 'CONTACT3',
                                     'mobile' => '18605819527',
                                     'time_use' => Carbon::now(),
                                     'location_start' => '青岛',
                                     'location_end' => '北京',
                                     'car_company_id' => $car_company1->id,
                                     'car_id' => App\Car::first()->id,
                                     'driver_id' => $driver1->id]);

        App\Order::create(['parent_order_id' => $parent->id,
                           'customer_id' => $customer1->id,
                           'order_type_id' => 'fullday',
                           'order_state_id' => 'w_drv_start_info',
                           'car_type_id' => 'suv',
                           'department' => 'DEPARTMENT3',
                           'contact' => 'CONTACT3',
                           'mobile' => '18605819527',
                           'time_use' => Carbon::now(),
                           'location_start' => '青岛',
                           'location_end' => '北京',
                           'car_company_id' => $car_company1->id,
                           'car_id' => App\Car::first()->id,
                           'driver_id' => $driver1->id]);

        // 价格策略
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'接送机 经济', 'base_price' => 160.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'接送机 舒适', 'base_price' => 180.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'接送机 商务7座', 'base_price' => 200.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'接送机 SUV', 'base_price' => 200.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'接送机 豪华', 'base_price' => 320.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'airport')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'接送机 14-55座中巴', 'base_price' => 360.00, 'overtime_price' => 0.00, 'overmile_price' => 0.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'半日租代驾 经济', 'base_price' => 260.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'半日租代驾 舒适', 'base_price' => 280.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'半日租代驾 商务7座', 'base_price' => 300.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'半日租代驾 SUV', 'base_price' => 320.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'半日租代驾 豪华', 'base_price' => 450.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'halfday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'半日租代驾 14-55座中巴', 'base_price' => 550.00, 'overtime_price' => 20.00, 'overmile_price' => 5.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'全日租代驾 经济', 'base_price' => 350.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'全日租代驾 舒适', 'base_price' => 460.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'全日租代驾 商务7座', 'base_price' => 480.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'全日租代驾 SUV', 'base_price' => 500.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'全日租代驾 豪华', 'base_price' => 600.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'cmb_14_55')->firstOrFail()->id,
                                   'name' =>'全日租代驾 14-55座中巴', 'base_price' => 850.00, 'overtime_price' => 20.00, 'overmile_price' => 5.00]);

        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'eco')->firstOrFail()->id,
                                   'name' =>'全日租自驾 经济', 'base_price' => 150.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'com')->firstOrFail()->id,
                                   'name' =>'全日租自驾 舒适', 'base_price' => 260.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'bus_7')->firstOrFail()->id,
                                   'name' =>'全日租自驾 商务7座', 'base_price' => 280.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'suv')->firstOrFail()->id,
                                   'name' =>'全日租自驾 SUV', 'base_price' => 300.00, 'overtime_price' => 20.00, 'overmile_price' => 2.00]);
        App\PriceStrategy::create(['order_type_id' => App\OrderType::where('type', 'fullday_self')->firstOrFail()->id,
                                   'car_type_id' => App\CarType::where('type', 'lux')->firstOrFail()->id,
                                   'name' =>'全日租自驾 豪华', 'base_price' => 400.00, 'overtime_price' => 20.00, 'overmile_price' => 4.00]);


        Model::reguard();
    }
}
