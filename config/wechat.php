<?php
return [
    'use_alias'    => env('WECHAT_USE_ALIAS', false),
    'app_id'       => env('WECHAT_APPID', 'wxf5061d3a767e82cb'), // 必填
    'secret'       => env('WECHAT_SECRET', '7f73a624d9855e275aa38cf44e7058db'), // 必填
    'token'        => env('WECHAT_TOKEN', 'pychat'),  // 必填
    'encoding_key' => env('WECHAT_ENCODING_KEY', 'YourEncodingAESKey') // 加密模式需要，其它模式不需要
];