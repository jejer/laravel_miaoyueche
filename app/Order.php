<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'time_use', 'time_final'];

    /**
     * Protect some field from mass assignment
     * @var array
     */
    protected $guarded = ['id', 'final_price', 'deleted_at', 'is_complain', 'created_at', 'updated_at'];

    public function getIsPickupAttribute($value)
    {
        if (is_null($value)) {
            return '0';
        }
        return $value;
    }
    public function getIsComplainAttribute($value)
    {
        if (is_null($value) or $value == False) {
            return '否';
        }
        return '是';
    }

    public function setCarTypeIdAttribute($value)
    {
        $this->attributes['car_type_id'] = CarType::where('type', $value)->firstOrFail()->id;
    }
    public function getCarTypeIdAttribute($value)
    {
        if (is_null($value)) {
            return CarType::first()->type;
        }
        return CarType::findOrFail($value)->type;
    }

    public function setOrderTypeIdAttribute($value)
    {
        $this->attributes['order_type_id'] = OrderType::where('type', $value)->firstOrFail()->id;
    }
    public function getOrderTypeIdAttribute($value)
    {
        if (is_null($value)) {
            return OrderType::first()->type;
        }
        return OrderType::findOrFail($value)->type;
    }

    public function setOrderStateIdAttribute($value)
    {
        $this->attributes['order_state_id'] = OrderState::where('state', $value)->firstOrFail()->id;
    }
    public function getOrderStateIdAttribute($value)
    {
        if (is_null($value)) {
            return OrderState::first()->state;
        }
        return OrderState::findOrFail($value)->state;
    }

    public function setTimeUseAttribute($date)
    {
        $this->attributes['time_use'] = Carbon::parse($date);
    }
    public function getTimeUseAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i');
    }



    public function order_type()
    {
        // has get attribute, relation not working
        // return $this->belongsTo('App\OrderType');
        return OrderType::findOrFail($this->getOriginal('order_type_id'));
    }

    public function order_state()
    {
        // has get attribute, relation not working
        // return $this->belongsTo('App\OrderState');
        return OrderState::findOrFail($this->getOriginal('order_state_id'));
    }

    public function car_type()
    {
        // has get attribute, relation not working
        // return $this->belongsTo('App\CarType');
        return CarType::findOrFail($this->getOriginal('car_type_id'));
    }


    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function car_company()
    {
        return $this->belongsTo('App\CarCompany');
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    public function child_orders() {
        return $this->hasMany('App\Order', 'parent_order_id');
    }

    public function parent_order()
    {
        return $this->belongsTo('App\Order');
    }

    public function order_charges() {
        return $this->hasMany('App\OrderCharge');
    }

    public function order_histories() {
        return $this->hasMany('App\OrderHistory');
    }

    public function scopeOfState($query, $state)
    {
        if ($state == 'waiting') {
            $state_id = OrderState::where('state', 'w_cs_asn')->firstOrFail()->id;
            return $query->where('order_state_id', $state_id);
        } elseif ($state == 'inprogress') {
            $state_id = OrderState::where('state', 'finished')->firstOrFail()->id;
            return $query->where('order_state_id', '!=', $state_id);
        } elseif ($state == 'completed') {
            $state_id = OrderState::where('state', 'finished')->firstOrFail()->id;
            return $query->where('order_state_id', $state_id);
        } 
    }

    public function scopeDriverVisiable($query)
    {
        $state_id = OrderState::where('state', 'w_cus_cfm_car')->firstOrFail()->id;
        return $query->where('order_state_id', '!=', $state_id);
    }

    public function scopeWithoutChild($query)
    {
        return $query->where('parent_order_id', null);
    }

    public function getPriceStrategy()
    {
        return \App\PriceStrategy::where('order_type_id', $this->getOriginal('order_type_id'))
                                 ->where('car_type_id', $this->getOriginal('car_type_id'))->firstOrFail();
    }

    public function getTotalPrice()
    {
        $total = 0.00;
        foreach ($this->order_charges as $charge) {
            $total += $charge->price * $charge->count;
        }
        return $total;
    }

    public function isAllChildCompleted() {
        if (!$this->child_orders()->count()) {
            return False;
        }
        
        $result = True;
        foreach ($this->child_orders as $child) {
            if ($child->order_state_id != 'finished') {
                $result = False;
                break;
            }
        }
        return $result;
    }
}
