<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, HasRoles;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'wx_openid'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Get userinfo, used by customer_service
     * @return [type] [description]
     */
    public function userinfo() {
        return $this->hasOne('App\Userinfo')->withTrashed();
    }

    public function op_logs() {
        return $this->hasMany('App\OpLog');
    }

    /**
     * Get customer company
     * @return [type] [description]
     */
    public function customer() {
        return $this->hasOne('App\Customer')->withTrashed();
    }

    /**
     * Get car company
     * @return [type] [description]
     */
    public function car_company() {
        return $this->hasOne('App\CarCompany')->withTrashed();
    }

    /**
     * Get driver
     * @return [type] [description]
     */
    public function driver() {
        return $this->hasOne('App\Driver')->withTrashed();
    }

    public function role() {
        if ($this->hasRole('customer_service')) {
            return '平台客服';
        }
        if ($this->hasRole('customer')) {
            return '客户';
        }
        if ($this->hasRole('car_company')) {
            return '租车公司';
        }
        if ($this->hasRole('driver')) {
            return '司机';
        }
    }
}
