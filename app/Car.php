<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['car_type_id', 'lpn', 'model', 'birth_year', 'pic_license', 'pic_insurance'];

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function car_company()
    {
        return $this->belongsTo('App\CarCompany');
    }

    public function lpn_model()
    {
        return $this->lpn.' '.$this->model;
    }
}
