<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userinfo extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name',
                           'mobile',
                           'id_number',
                           'pic_avatar',
                           'pic_id_1',
                           'pic_id_2',
                           'note'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
