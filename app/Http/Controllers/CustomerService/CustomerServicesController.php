<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CustomerServicesController extends Controller
{
    public function deleted()
    {
        $page_name = '被删除客服';
        $customer_services = \App\Role::ofName('customer_service')->firstOrFail()->users()->onlyTrashed()->paginate(20);
        
        return view('cs.customer_services', compact('customer_services', 'page_name'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = '所有客服';
        $customer_services = \App\Role::ofName('customer_service')->firstOrFail()->users()->paginate(20);

        return view('cs.customer_services', compact('customer_services', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cs.customer_service_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Input::get('balance'));
        if (\App\User::where('email', $request->input('email'))->count() > 0) {
            Session::flash('flash_message', 'Email 已被注册');
            return back()->withInput();
        }

        DB::transaction(function() use ($request) {
            $user = \App\User::create(['email'=>$request->input('email'), 
                                       'name'=>$request->input('username'), 
                                       'password'=>bcrypt($request->input('password'))]);
            $user->assignRole('customer_service');
            $user->userinfo()->create($request->input());
            Session::flash('flash_message', '客服添加成功');
        });

        return redirect('/cs/customer_services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::withTrashed()->findOrFail($id);
        
        return view('cs.customer_service_edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::findOrFail($id);
        $user->name = $request->input('username');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        // \DB::enableQueryLog();
        $user->userinfo->update($request->input());
        // dd(\DB::getQueryLog());

        return redirect('/cs/customer_services')->with('flash_message', '客服更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use ($id) {
            $user = \App\User::findOrFail($id);
            $user->userinfo->delete();
            $user->delete();
            Session::flash('flash_message', '客服删除成功');
        });

        return redirect('/cs/customer_services');
    }

}
