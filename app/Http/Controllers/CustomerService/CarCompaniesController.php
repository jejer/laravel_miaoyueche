<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CarCompaniesController extends Controller
{
    public function deleted()
    {
        $page_name = '被删除租车公司';
        $car_companies = \App\CarCompany::onlyTrashed()->paginate(20);
        
        return view('cs.car_companies', compact('car_companies', 'page_name'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = '所有租车公司';
        $car_companies = \App\CarCompany::paginate(20);

        return view('cs.car_companies', compact('car_companies', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cs.car_company_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Input::get('balance'));
        if (\App\User::where('email', $request->input('email'))->count() > 0) {
            Session::flash('flash_message', 'Email 已被注册');
            return back()->withInput();
        }

        if (\App\CarCompany::where('name', $request->input('name'))->count() > 0) {
            Session::flash('flash_message', '公司名称 已被注册');
            return back()->withInput();
        }

        DB::transaction(function ($request) use ($request) {
            $user = \App\User::create(['email'=>$request->input('email'), 
                                       'name'=>$request->input('username'), 
                                       'password'=>bcrypt($request->input('password'))]);
            $user->assignRole('car_company');
            $user->car_company()->create($request->input());
            Session::flash('flash_message', '租车公司添加成功');
        });

        return redirect('/cs/car_companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\CarCompany::withTrashed()->findOrFail($id)->user()->withTrashed()->firstOrFail();
        
        return view('cs.car_company_edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::findOrFail($id);
        $user->name = $request->input('username');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        // \DB::enableQueryLog();
        $user->car_company->update($request->input());
        // dd(\DB::getQueryLog());

        return redirect('/cs/car_companies')->with('flash_message', '租车公司更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use ($id) {
            $user = \App\User::findOrFail($id);
            $user->car_company->delete();
            $user->delete();
            Session::flash('flash_message', '租车公司删除成功');
        });

        return redirect('/cs/car_companies');
    }

    public function resources($id)
    {
        $car_company = \App\CarCompany::findOrFail($id);

        return $car_company->cars()->lists('model', 'lpn');
    }

}
