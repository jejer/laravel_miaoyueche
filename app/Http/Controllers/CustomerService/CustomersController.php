<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CustomersController extends Controller
{
    public function deleted()
    {
        $page_name = '被删除客户';
        $customers = \App\Customer::onlyTrashed()->paginate(20);
        
        return view('cs.customers', compact('customers', 'page_name'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = '所有客户';
        $customers = \App\Customer::paginate(20);

        return view('cs.customers', compact('customers', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cs.customer_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Input::get('balance'));
        if (\App\User::where('email', $request->input('email'))->count() > 0) {
            Session::flash('flash_message', 'Email 已被注册');
            return back()->withInput();
        }

        if (\App\Customer::where('name', $request->input('name'))->count() > 0) {
            Session::flash('flash_message', '公司名称 已被注册');
            return back()->withInput();
        }

        DB::transaction(function ($request) use ($request) {
            $user = \App\User::create(['email'=>$request->input('email'), 
                                       'name'=>$request->input('username'), 
                                       'password'=>bcrypt($request->input('password'))]);
            $user->assignRole('customer');
            $user->customer()->create($request->input());
            $user->customer->balance = $request->input('balance');
            $user->customer->save();
            Session::flash('flash_message', '客户添加成功');
        });

        return redirect('/cs/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Customer::withTrashed()->findOrFail($id)->user()->withTrashed()->firstOrFail();
        
        return view('cs.customer_edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id \App\User id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::findOrFail($id);
        $user->name = $request->input('username');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        // \DB::enableQueryLog();
        $user->customer->update($request->input());
        if ($user->customer->balance != $request->input('balance')) {
            \Auth::user()->op_logs()->create(
                ['detail' => sprintf('修改客户[%s]储值余额[%d]->[%d]',
                    $user->customer->name, $user->customer->balance, $request->input('balance'))]);
            $user->customer->balance = $request->input('balance');
        }
        
        $user->customer->save();
        // dd(\DB::getQueryLog());

        return redirect('/cs/customers')->with('flash_message', '客户更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function($id) use ($id) {
            $user = \App\User::findOrFail($id);
            $user->customer->delete();
            $user->delete();
            Session::flash('flash_message', '客户删除成功');
        });

        return redirect('/cs/customers');
    }

}
