<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class CarsController extends Controller
{
    public function deleted()
    {
        $page_name = '被删除车辆';
        $cars = \App\Car::onlyTrashed()->paginate(20);

        return view('cs.cars', compact('cars', 'page_name'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = '所有车辆';
        $cars = \App\Car::paginate(20);

        return view('cs.cars', compact('cars', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $car_companies = \App\CarCompany::lists('name', 'id');
        return view('cs.car_create', compact('car_companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function() use ($request) {
            $car_company = \App\CarCompany::findOrFail($request->input('car_company_id'));
            $car_company->cars()->create(array_merge(['car_type_id'=>\App\CarType::firstOrFail()->id], $request->input()));
        });

        return redirect('/cs/cars')->with('flash_message', '添加车辆成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = \App\Car::withTrashed()->findOrFail($id);
        $car_companies = \App\CarCompany::lists('name', 'id');
        return view('cs.car_edit', compact('car_companies', 'car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $car = \App\Car::findOrFail($id);

        if ($car->car_company_id != $request->input('car_company_id')) {
            return redirect('/cs/cars')->with('flash_message', '编辑车辆失败，车辆所属公司不可变更');
        }

        DB::transaction(function() use ($request, $car) {
            $car_company = \App\CarCompany::findOrFail($request->input('car_company_id'));
            $car->car_company()->associate($car_company);
            $car->update($request->input());
        });

        return redirect('/cs/cars')->with('flash_message', '编辑车辆成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use ($id) {
            $car = \App\Car::findOrFail($id);
            $car->delete();
            \Session::flash('flash_message', '车辆删除成功');
        });

        return redirect('/cs/cars');
    }
}
