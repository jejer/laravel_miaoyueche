<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class DriversController extends Controller
{
    public function deleted()
    {
        $page_name = '被删除司机';
        $drivers = \App\Driver::onlyTrashed()->paginate(20);
        
        return view('cs.drivers', compact('drivers', 'page_name'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = '所有司机';
        $drivers = \App\Driver::paginate(20);

        return view('cs.drivers', compact('drivers', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cs.driver_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(Input::get('balance'));
        if (\App\User::where('email', $request->input('email'))->count() > 0) {
            Session::flash('flash_message', 'Email 已被注册');
            return back()->withInput();
        }

        if (\App\Userinfo::where('id_number', $request->input('id_number'))->count() > 0) {
            Session::flash('flash_message', '身份证号码 已被注册');
            return back()->withInput();
        }

        DB::transaction(function ($request) use ($request) {
            $user = \App\User::create(['email'=>$request->input('email'), 
                                       'name'=>$request->input('username'), 
                                       'password'=>bcrypt($request->input('password'))]);
            $user->assignRole('driver');
            $user->driver()->create($request->input());
            Session::flash('flash_message', '司机添加成功');
        });

        return redirect('/cs/drivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Driver::withTrashed()->findOrFail($id)->user()->withTrashed()->firstOrFail();
        
        return view('cs.driver_edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::findOrFail($id);
        $user->name = $request->input('username');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        // \DB::enableQueryLog();
        $user->driver->update($request->input());
        // dd(\DB::getQueryLog());

        return redirect('/cs/drivers')->with('flash_message', '司机更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use ($id) {
            $user = \App\User::findOrFail($id);
            $user->userinfo->delete();
            $user->delete();
            Session::flash('flash_message', '司机删除成功');
        });

        return redirect('/cs/drivers');
    }

}
