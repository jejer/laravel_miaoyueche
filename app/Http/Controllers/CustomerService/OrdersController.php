<?php

namespace App\Http\Controllers\CustomerService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Event;
use App\Events\OrderCarAssignedEvent;

use App\Http\Controllers\OrderController;


class OrdersController extends Controller
{
    public function orders($state)
    {
        if ($state == 'waiting') {
            $orders = \App\Order::ofState($state)->withoutchild()->paginate(20);
            $page_name = '待配车订单';
        } elseif ($state == 'inprogress') {
            $orders = \App\Order::ofState($state)->withoutchild()->paginate(20);
            $page_name = '未完成订单';
        } elseif ($state == 'completed') {
            $orders = \App\Order::ofState($state)->withoutchild()->paginate(20);
            $page_name = '已完成订单';
        } elseif ($state == 'all') {
            $orders = \App\Order::withoutchild()->paginate(20);
            $page_name = '所有订单';
        } elseif ($state == 'deleted') {
            $orders = \App\Order::onlyTrashed()->withoutchild()->paginate(20);
            $page_name = '回收站';
        }

        return view('cs.orders', compact('state', 'orders', 'page_name'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/cs/orders/state/waiting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = \App\Order::withTrashed()->findOrFail($id);

        $buttons = [];
        if (!$order->trashed()) {
            $buttons['删除订单'] = url('/cs/orders', $order->id).'/delete';

            if ($order->order_state_id != 'finished') {
                $buttons['增减附加费用'] = url('/cs/orders', $order->id).'/add_charge';
                $buttons['标记/取消投诉'] = url('/cs/orders', $order->id).'/complain';
            }

            if ($order->order_state_id == 'w_cs_asn') {
                $buttons['配车'] = url('/cs/orders', $order->id).'/assign';
            }
            if ($order->order_state_id == 'w_cus_cfm_car') {
                $buttons['重新配车'] = url('/cs/orders', $order->id).'/assign';
                $buttons['代客户确认用车'] = url('/cs/orders', $order->id).'/cfm_car';
            }
        }

        return view('cs.order', compact('order', 'buttons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // User can not edit, only customer_service can.
        abort(404);
        $order = \App\Order::findOrFail($id);
        
        return view('cs.order_edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // User can not edit, only customer_service can.
        abort(404);
        $order = \App\Order::findOrFail($id);
        $order->update($request->input());

        return redirect('/cs/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = \App\Order::findOrFail($id);

        $order->delete();
        return redirect('/cs/orders')->with('flash_message', '订单删除成功');
    }

    public function assign(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);
        $car_companies = \App\CarCompany::lists('name', 'id');
        $drivers = \App\Driver::lists('name', 'id');

        if ($request->isMethod('get')) {
            return view('cs.order_assign', compact('order', 'car_companies', 'drivers'));
        } else if ($request->isMethod('post')) {
            // dd($request->input());
            DB::transaction(function() use ($order, $request) {
                $order->car_company()->associate(\App\CarCompany::findOrFail($request->input('car_company_id')));
                $order->car()->associate(\App\Car::where('lpn', $request->input('car_lpn'))->firstOrFail());
                $order->driver()->associate(\App\Driver::findOrFail($request->input('driver_id')));
                $order->order_state_id = 'w_cus_cfm_car';
                $order->save();
            });

            $order->order_histories()->create(['operator' => \Auth::user()->userinfo->name, 'info' => '客服已配车：'.$order->car->lpn.' 司机：'.$order->driver->name]);
            Event::fire(new OrderCarAssignedEvent($order));
            return redirect('/cs/orders')->with('flash_message', '订单配车成功');
        }
    }

    public function addCharge(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            return view('cs.order_add_charge', compact('order'));
        } else if ($request->isMethod('post')) {
            $order->order_charges()->create($request->input());
            $order->order_histories()->create(['operator' => $request->user()->userinfo->name, 'info' => '添加收费项目：'.$request->input('name')]);
            return redirect(url('/cs/orders', $id))->with('flash_message', '附加费用添加成功');
        }
    }

    public function complain(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            return view('cs.order_complain', compact('order'));
        } else if ($request->isMethod('post')) {
            if ($order->getOriginal('is_complain')) {
                $order->is_complain = False;
                $order->order_histories()->create(['operator' => $request->user()->userinfo->name, 'info' => '取消投诉，原因：'.$request->input('reason')]);
            } else {
                $order->is_complain = True;
                $order->order_histories()->create(['operator' => $request->user()->userinfo->name, 'info' => '标记投诉，原因：'.$request->input('reason')]);
            }
            
            $order->save();
            return redirect(url('/cs/orders', $id))->with('flash_message', '标记/取消投诉成功');
        }
    }

    public function comfirmCar($id)
    {
        $order = \App\Order::findOrFail($id);

        OrderController::orderCfmCar($order);
        $order->order_histories()->create(['operator' => \Auth::user()->userinfo->name, 'info' => '代客户确认用车']);
        return redirect('/cs/orders')->with('flash_message', '成功代客户确认用车');
    }

    public function add(Request $request)
    {
        if ($request->isMethod('get')) {
            $customers = \App\Customer::lists('name', 'id');
            return view('cs.order_add_select', compact('customers'));
        } elseif ($request->isMethod('post')) {
            $order_type = $request->input('order_type');
            $customer_id = $request->input('customer_id');
            if ($order_type == 'airport') {
                return redirect(url('/cs/orders/add_airport', $customer_id));
            } else {
                return redirect(url('/cs/orders/add_dayrent/'.$order_type, $customer_id));
            }
            
        }
    }

    /**
     * 新接送机订单
     * @return [type] [description]
     */
    public function toAirport(Request $request, $customer_id)
    {
        $customer = \App\Customer::findOrFail($customer_id);
        if ($request->isMethod('get')) {
            return view('cs.order_create_2airport', compact('customer'));
        } else if ($request->isMethod('post')) {
            $order = OrderController::newOrder($customer, $request->input());
            $order->order_histories()->create(['operator' => \Auth::user()->userinfo->name, 'info' => '代客下单']);
            
            return redirect('/cs/orders')->with('flash_message', '订单创建成功');
        }
    }

    public function dayRent(Request $request, $order_type, $customer_id)
    {
        $customer = \App\Customer::findOrFail($customer_id);
        if ($request->isMethod('get')) {
            if ($order_type == 'halfday') {
                return view('cs.order_create_halfday', compact('customer'));
            } elseif ($order_type == 'fullday') {
                return view('cs.order_create_fullday', compact('customer'));
            } else { abort(404); }
        } else if ($request->isMethod('post')) {
            $input = $request->input();
            // dd($input);
            if ($order_type == 'halfday') {
                $input['order_type_id'] = 'halfday';
            } elseif ($order_type == 'fullday') {
                $input['order_type_id'] = 'fullday';
                if (array_key_exists('is_self', $input)) {
                    $input['order_type_id'] = 'fullday_self';
                    unset($input['is_self']);
                }
            } else { abort(404); }

            $order = OrderController::newOrder($customer, $input);
            $order->order_histories()->create(['operator' => \Auth::user()->userinfo->name, 'info' => '代客下单']);
            return redirect('/cs/orders')->with('flash_message', '订单创建成功');
        }
    }

    public function billsSelect(Request $request)
    {
        if ($request->isMethod('get')) {
            $customers = \App\Customer::lists('name', 'id');
            $car_companies = \App\CarCompany::lists('name', 'id');
            return view('cs.bills_select', compact('customers', 'car_companies'));
        } elseif ($request->isMethod('post')) {
            $type = $request->input('type');
            $id = 0;
            if ($type == 'by_customer') {
                $id = $request->input('customer_id');
            }
            if ($type == 'by_car_company') {
                $id = $request->input('car_company_id');
            }
            return redirect('/cs/bills/'.$type.'/'.$id);
        }
    }

    public function bills($type, $id)
    {
        $page_name = "对账单查询 所有订单";
        if ($type == 'by_customer') {
            $customer = \App\Customer::findOrFail($id);
            $page_name = "对账单查询 按客户:".$customer->name;
        }
        if ($type == 'by_car_company') {
            $car_company = \App\CarCompany::findOrFail($id);
            $page_name = "对账单查询 按租车公司:".$car_company->name;
        }

        return view('cs.bills', compact('page_name', 'type', 'id'));
    }

    public function queryBills($type, $id, $start, $end)
    {
        // $start = \Carbon\Carbon::parse($start);
        $end = \Carbon\Carbon::parse($end);
        
        if ($type == 'by_customer') {
            $data = \App\Customer::findOrFail($id)->orders()->where('time_final', '>', $start)->where('time_final', '<', $end->addDay()->toDateString())->select('id','order_type_id','time_final','price_final')->paginate(100);
            return $data;
        }
        if ($type == 'by_car_company') {
            $data = \App\CarCompany::findOrFail($id)->orders()->where('time_final', '>', $start)->where('time_final', '<', $end->addDay()->toDateString())->select('id','order_type_id','time_final','price_final')->paginate(100);
            return $data;
        }
        if ($type == 'all') {
            $data = \App\Order::where('time_final', '>', $start)->where('time_final', '<', $end->addDay()->toDateString())->select('id','order_type_id','time_final','price_final')->paginate(100);
            return $data;
        }
    }
}
