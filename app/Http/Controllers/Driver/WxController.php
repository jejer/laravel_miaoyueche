<?php

namespace App\Http\Controllers\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Overtrue\Wechat\Auth;

class WxController extends Controller
{
    public function bind(Request $request, Auth $auth)
    {
        if (!$request->session()->has('wx_openid')) {
            $wx_user = $auth->authorize(null, 'snsapi_base', 'STATE');
            $request->session()->put('wx_openid', $wx_user['openid']);
        }

        $wx_openid = $request->session()->get('wx_openid');

        if ($request->isMethod('get')) {
            $user = \App\User::where('wx_openid', $wx_openid)->first();
            if ($user) {
                return "已绑定，无需重新绑定，请关闭页面";
            }
            return view('driver.wx_bind');
        } else if ($request->isMethod('post')) {
            // dd($request->input());
            $driver = \App\Driver::where('name', $request->input('name'))->where('id_number', $request->input('id_number'))->first();
            if (!$driver or !$driver->user->hasRole('driver') or $driver->user->wx_openid) {
                return "绑定错误, 请联系客服. 代码:".$wx_openid;
            }
            $driver->user()->update(['wx_openid' => $wx_openid]);

            return "绑定成功, 请关闭页面";
        }
    }
}
