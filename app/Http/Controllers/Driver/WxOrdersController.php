<?php

namespace App\Http\Controllers\Driver;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Controllers\OrderController;

class WxOrdersController extends Controller
{
    public function __construct(Request $request)
    {
        $wx_openid = $request->session()->get('wx_openid');
        $this->driver = \App\User::where('wx_openid', $wx_openid)->first()->driver;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->driver->orders()->withoutchild()->driverVisiable()->orderBy('id', 'desc')->limit(20)->get();
        return view('driver.wx_orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = \App\Order::findOrFail($id);

        $buttons = [];
        if ($order->order_state_id != 'finished') {
            $buttons = ['添加附加收费' => url('/wx/driver/orders', $id).'/add_charge'];
        }
        $form = '';
        if ($order->order_type_id == 'airport') {
            if ($order->order_state_id == 'w_cus_cfm_fin') {
                $form = 'final_sign';
                // $buttons['客户签名完成订单'] = url('/wx/driver/orders', $id).'/sign';
            }
        } elseif ($order->order_type_id == 'halfday' or $order->order_type_id == 'fullday_self'
                  or ($order->order_type_id == 'fullday' and $order->parent_order_id)) {
            if ($order->order_state_id == 'w_drv_start_info') {
                $form = 'start_info';
            } elseif ($order->order_state_id == 'w_cus_cfm_pickup') {
                $form = 'pickup_sign';
                $buttons['更改起始信息'] = url('/wx/driver/orders', $id).'/add_info';
            } elseif ($order->order_state_id == 'w_drv_fin_info') {
                $form = 'end_info';
            } elseif ($order->order_state_id == 'w_cus_cfm_fin') {
                $form = 'final_sign';
                $buttons['更改结束信息'] = url('/wx/driver/orders', $id).'/add_info';
            }
        } elseif ($order->order_type_id == 'fullday' and !$order->parent_order_id and $order->order_state_id != 'finished') {
            $form = 'add_child';
            if ($order->isAllChildCompleted()) {
                $buttons['完成本订单'] = url('/wx/driver/orders', $id).'/complete_parent';
            }
        }

        return view('driver.wx_order', compact('order', 'form', 'buttons'));
    }

    public function signature(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            abort(404);
        } elseif ($request->isMethod('post')) {
            // dd($request->input());

            // 保存签名
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('sig2')));
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            $uploadName = date('Y_m_d_His').'_'.$randomString.'.png';
            $sign_img = uniqid('SIGN_' . date('Y_m_d_Hi') . '_') . '.png';
            $directory = public_path().'/uploads/signatures/';
            if (!is_dir($directory)) {
                // dir doesn't exist, make it
                mkdir($directory);
            }
            file_put_contents($directory . $sign_img, $data);

            if ($order->order_state_id == 'w_cus_cfm_fin') {
                $order->update(['sign_final' => $sign_img]);
                OrderController::orderComplete($order);

                return redirect('/wx/driver/orders');
            }

            if ($order->order_state_id == 'w_cus_cfm_pickup') {
                $order->sign_pickup = $sign_img;
                $order->order_state_id = 'w_drv_fin_info';
                $order->save();

                $order->order_histories()->create(['operator' => '客户', 'info' => '签名确认起始信息']);

                return redirect('/wx/driver/orders');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }

    public function addCharge(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            return view('driver.wx_order_add_charge', compact('order'));
        } else if ($request->isMethod('post')) {
            $order->order_charges()->create($request->input());
            $order->order_histories()->create(['operator' => $this->driver->name, 'info' => '添加收费项目：'.$request->input('name')]);
            return redirect(url('/wx/driver/orders', $id))->with('flash_message', '附加费用添加成功');
        }
    }

    public function addInfo(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            if ($order->order_state_id == 'w_drv_start_info' or $order->order_state_id == 'w_cus_cfm_pickup') {
                $form = 'start_info';
                return view('driver.wx_order_add_info', compact('order', 'form'));
            }
            if ($order->order_state_id == 'w_drv_fin_info' or $order->order_state_id == 'w_cus_cfm_fin') {
                $form = 'end_info';
                return view('driver.wx_order_add_info', compact('order', 'form'));
            }
        } else if ($request->isMethod('post')) {
            
            if ($order->order_state_id == 'w_drv_start_info' or $order->order_state_id == 'w_cus_cfm_pickup') {
                $order->update($request->input());
                $order->order_state_id = 'w_cus_cfm_pickup';
                $order->save();

                $order->order_histories()->create(['operator' => $this->driver->name, 'info' => '司机录入起始信息']);
            }

            if ($order->order_state_id == 'w_drv_fin_info' or $order->order_state_id == 'w_cus_cfm_fin') {
                $order->update($request->input());
                $order->order_state_id = 'w_cus_cfm_fin';
                $order->save();

                $order->order_histories()->create(['operator' => $this->driver->name, 'info' => '司机录入结束信息']);
            }
            return redirect(url('/wx/driver/orders', $id));
        }
    }

    /**
     * 增加全日租代驾子订单
     * @param Request $request [description]
     * @param [type]  $id      [description]
     */
    public function addChild(Request $request, $id) 
    {
        $order = \App\Order::findOrFail($id);

        if ($request->isMethod('get')) {
            abort(404);
        }

        $child = OrderController::newChildOrder($order, $request->input('child_date'), $this->driver->name);

        return redirect(url('/wx/driver/orders', $child->id));
    }

    public function completeParent($id) {
        $order = \App\Order::findOrFail($id);

        if ($order->isAllChildCompleted()) {
            OrderController::orderComplete($order);

            return redirect('/wx/driver/orders');
        }
    }
}
