<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Event;
use App\Events\NewOrderEvent;
use App\Events\OrderCarComfirmedEvent;

use App\Http\Controllers\OrderController;


class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // \DB::enableQueryLog();
        // var_dump(\App\Order::first()->order_type()->get());
        // var_dump(\DB::getQueryLog());
        $orders = Auth::user()->customer->orders()->withoutchild()->orderBy('id', 'desc')->paginate(20);
        // dd($orders);
        return view('cu.orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = \App\Order::findOrFail($id);

        return view('cu.order', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // User can not edit, only customer_service can.
        abort(404);
        $order = \App\Order::findOrFail($id);
        
        return view('cu.order_edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // User can not edit, only customer_service can.
        abort(404);
        $order = \App\Order::findOrFail($id);
        $order->update($request->input());

        return redirect('/cu/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }

    /**
     * 新订单类型选择
     * @return [type] [description]
     */
    public function orderTypeSelect()
    {
        return view('cu.order_type_select');
    }

    /**
     * 新接送机订单
     * @return [type] [description]
     */
    public function toAirport(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('cu.order_create_2airport');
        } else if ($request->isMethod('post')) {
            $order = OrderController::newOrder(Auth::user()->customer, $request->input());
            $order->order_histories()->create(['operator' => '客户', 'info' => '您提交了订单，请等待客服配车']);
            
            return redirect('/cu/orders')->with('flash_message', '订单创建成功');
        }
    }

    public function dayRent(Request $request, $type)
    {
        if ($request->isMethod('get')) {
            if ($type == 'halfday') {
                return view('cu.order_create_halfday');
            } elseif ($type == 'fullday') {
                return view('cu.order_create_fullday');
            } else { abort(404); }
        } else if ($request->isMethod('post')) {
            $input = $request->input();
            // dd($input);
            if ($type == 'halfday') {
                $input['order_type_id'] = 'halfday';
            } elseif ($type == 'fullday') {
                $input['order_type_id'] = 'fullday';
                if (array_key_exists('is_self', $input)) {
                    $input['order_type_id'] = 'fullday_self';
                    unset($input['is_self']);
                }
            } else { abort(404); }

            $order = OrderController::newOrder(Auth::user()->customer, $input);
            $order->order_histories()->create(['operator' => '客户', 'info' => '您提交了订单，请等待客服配车']);
            return redirect('/cu/orders')->with('flash_message', '订单创建成功');
        }
    }

    public function action(Request $request, $id, $action)
    {
        $order = \App\Order::findOrFail($id);

        if ($action == 'cfm_car') {
            OrderController::orderCfmCar($order);
            $order->order_histories()->create(['operator' => '客户', 'info' => '您已确认用车']);
            return redirect('/cu/orders')->with('flash_message', '订单成功确认用车');
        }
    }

    public function bills()
    {
        return view('cu.bills');
    }

    public function queryBills($start, $end)
    {
        // $start = \Carbon\Carbon::parse($start);
        $end = \Carbon\Carbon::parse($end);
        // \DB::enableQueryLog();
        $data = \Auth::user()->customer->orders()->where('time_final', '>', $start)->where('time_final', '<', $end->addDay()->toDateString())->select('id','order_type_id','time_final','price_final')->paginate(100);
        // var_dump(\DB::getQueryLog());
        return $data;
    }
}
