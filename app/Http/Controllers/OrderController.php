<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Event;
use App\Events\NewOrderEvent;
use App\Events\OrderCarComfirmedEvent;
use App\Events\OrderCompletedEvent;

class OrderController extends Controller
{
    static function orderComplete($order)
    {
        DB::transaction(function() use ($order) {
            //添加日志
            $order->order_histories()->create(['operator' => '客户', 'info' => '签名确认订单完成']);

            //计算价格
            OrderController::calcOverPrices($order);
            $order->price_final = $order->getTotalPrice();
            
            //变更订单状态
            $order->order_state_id = 'finished';
            $order->time_final = \Carbon\Carbon::now();

            //保存到数据库
            $order->save();

            $parent = \App\Order::find($order->parent_order_id);
            if ($parent) {
                //增加父订单日志（子订单完成）
                $child_date = \Carbon\Carbon::parse($order->getOriginal('time_use'))->format('Y-m-d');
                $parent->order_histories()->create(['operator' => '系统', 'info' => $child_date.'日用车完成']);

                //增加父订单 收费
                $parent->order_charges()->create(['name'=>$child_date.'日用车收费', 'price'=>$order->price_final]);
            } else {
                //增加积分
                $order->customer->point = $order->customer->point + intval($order->price_final);
                $order->customer->save();
                //微信通知司机
                Event::fire(new OrderCompletedEvent($order));
            }  
        });
    }

    static function orderCfmCar($order)
    {
        if ($order->order_type_id == 'airport') {
            $next_state = 'w_cus_cfm_fin';
        } else {
            $next_state = 'w_drv_start_info';
        }

        $order->order_state_id = $next_state;
        $order->save();

        Event::fire(new OrderCarComfirmedEvent($order));
    }

    static function newOrder($customer, $input)
    {
        return DB::transaction(function() use ($customer, $input) {
            $order = $customer->orders()->create(array_merge(['order_state_id'=>'w_cs_asn'],$input));
            if ($order->order_type_id == 'airport') {
                $price_strategy = $order->getPriceStrategy();
                $order->order_charges()->create(['name' => '接送机 车型：'.$order->car_type()->name, 'price' => $price_strategy->base_price]);
            } elseif ($order->order_type_id == 'halfday') {
                $price_strategy = $order->getPriceStrategy();
                $order->order_charges()->create(['name' => '半日租代驾 车型：'.$order->car_type()->name, 'price' => $price_strategy->base_price]);
            }
            Event::fire(new NewOrderEvent($order));
            return $order;
        });
    }

    static function newChildOrder($order, $child_date, $driver_name)
    {
        return DB::transaction(function() use ($order, $child_date, $driver_name) {
            $child = $order->child_orders()->create(array_intersect_key($order->toArray(), 
                array_flip(['customer_id', 'order_type_id', 'order_state_id', 'car_type_id', 'department',
                'contact', 'mobile', 'location_start', 'location_end', 'car_company_id', 'car_id', 'driver_id'])));
            $child->time_use = $child_date;
            $child->save();

            $child->order_histories()->create(['operator' => $driver_name, 'info' => '司机创建子订单']);
            $order->order_histories()->create(['operator' => $driver_name, 'info' => '司机创建'.$child_date.'日用车子订单']);

            $price_strategy = $child->getPriceStrategy();
            $child->order_charges()->create(['name' => '日租每日收费', 'price' => $price_strategy->base_price]);
            return $child;
        });
    }

    static function calcOverPrices($order) {
        $ps = $order->getPriceStrategy();

        $miles = ceil($order->mile_end - $order->mile_start);
        $seconds = strtotime($order->time_end) - strtotime($order->time_start);
        $days = floor($seconds / (60*60*24));
        $hours = floor( $seconds / (60*60) );
        $hours_left = ceil( ( $seconds - (60*60*24)*$days ) / (60*60) );

        // 半日租代驾 100km 4hr
        if ($order->order_type_id == 'halfday') {
            if ($miles > 100) {
                $order->order_charges()->create(['name' => '超公里 加收费', 'price' => $ps->overmile_price, 'count' => $miles-100, 'count_unit' => '公里']);
            }
            
            if ($hours > 4) {
                $order->order_charges()->create(['name' => '超小时 加收费', 'price' => $ps->overtime_price, 'count' => $hours-4, 'count_unit' => '小时']);
            }
        }

        // 全日租代驾 (每个子订单) 100km 8hr (每日收费在子订单创建时添加)
        if ($order->order_type_id == 'fullday') {
            if ($miles > 100) {
                $order->order_charges()->create(['name' => '超公里 加收费', 'price' => $ps->overmile_price, 'count' => $miles-100, 'count_unit' => '公里']);
            }
            
            if ($hours > 8) {
                $order->order_charges()->create(['name' => '超小时 加收费', 'price' => $ps->overtime_price, 'count' => $hours-8, 'count_unit' => '小时']);
            }
        }

        // 全日租自驾
        if ($order->order_type_id == 'fullday_self') {

            // 不足一天算一天
            if ($days < 1) {
                $days = 1;
                $hours = 0;
            }

            // 超4小时另算一天
            if ($hours_left > 4) {
                $days += 1;
                $hours_left = 0;
            }

            $order->order_charges()->create(['name' => '日租每日收费', 'price' => $ps->base_price, 'count' => $days, 'count_unit' => '天']);
            
            if ($hours_left > 0) {
                $order->order_charges()->create(['name' => '超小时 加收费', 'price' => $ps->overtime_price, 'count' => $hours, 'count_unit' => '小时']);
            }
        }
        
    }
}
