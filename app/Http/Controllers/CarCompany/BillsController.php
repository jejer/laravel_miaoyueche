<?php

namespace App\Http\Controllers\CarCompany;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BillsController extends Controller
{
    public function bills()
    {
        return view('cc.bills');
    }

    public function queryBills($start, $end)
    {
        // $start = \Carbon\Carbon::parse($start);
        $end = \Carbon\Carbon::parse($end);
        // \DB::enableQueryLog();
        $data = \Auth::user()->car_company->orders()->where('time_final', '>', $start)->where('time_final', '<', $end->addDay()->toDateString())->select('id','order_type_id','time_final','price_final')->paginate(100);
        // var_dump(\DB::getQueryLog());
        return $data;
    }
}
