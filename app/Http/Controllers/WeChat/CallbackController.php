<?php

namespace App\Http\Controllers\WeChat;

use App\Http\Controllers\Controller;

use Overtrue\Wechat\Server;
use Overtrue\Wechat\Menu;
use Overtrue\Wechat\MenuItem;

use Log;

class CallbackController extends Controller
{
    public function serve(Server $server)
    {
        $server->on('message', function($message){
            Log::info($message);
            return "欢迎关注！";
        });

        return $server->serve(); // 或者 return $server;
    }

    public function clearMenu(Menu $menu)
    {
        $menu->delete();
        return __FUNCTION__;
    }

    public function setWxMenu(Menu $menu)
    {
        $button = new MenuItem("其他");

        $menus = array(
            new MenuItem("绑定", 'view', 'http://www.miaoyueche.com/wx/cs/bind'),
            new MenuItem("我的订单", 'view', 'http://www.miaoyueche.com/wx/cs/orders'),
            $button->buttons(array(
                    new MenuItem('司机绑定', 'view', 'http://www.miaoyueche.com/wx/driver/bind'),
                    new MenuItem('司机订单', 'view', 'http://www.miaoyueche.com/wx/driver/orders'),
                )),
        );

        $menu->set($menus);

        return "OK";
    }
}