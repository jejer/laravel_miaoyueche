<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    public function carInfo($id) {
        $car = \App\Car::findOrFail($id);

        if (\Auth::user()->hasRole('customer_service')) {
            return view('cs.car', compact('car'));
        } elseif (\Auth::user()->hasRole('customer')) {
            return view('cu.car', compact('car'));
        }
    }

    public function driverInfo($id) {
        $driver = \App\Driver::findOrFail($id);

        if (\Auth::user()->hasRole('customer_service')) {
            return view('cs.driver', compact('driver'));
        } elseif (\Auth::user()->hasRole('customer')) {
            return view('cu.driver', compact('driver'));
        }
    }
}
