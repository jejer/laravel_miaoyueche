<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    public function upload(Request $request, $type)
    {
        if(!$request->hasFile('qqfile') or !$request->file('qqfile')->isValid()) {
          return response()->json(['success'=>False], 200);
        }

        $img = \Image::make($request->file('qqfile'));
        $img->resize(null, 720, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $filePath = public_path().'/uploads/'.$type.'/';
        if (!is_dir($filePath)) {
            // dir doesn't exist, make it
            mkdir($filePath);
        }

        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $uploadName = date('Y_m_d_His').'_'.$randomString.'.'.$request->file('qqfile')->getClientOriginalExtension();
        $img->save($filePath . $uploadName);

        $success = True;
        return response()->json(compact('success', 'uploadName'), 200);
    }
}
