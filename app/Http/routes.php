<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'TestController@index');


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');



Route::group(['middleware' => 'require_role:customer', 'prefix' => 'cu'], function() {
	Route::get('orders/create/order_type_select', 'Customer\OrdersController@orderTypeSelect');
    Route::any('orders/create/to_airport', 'Customer\OrdersController@toAirport');
    Route::any('orders/create/day_rent/{type}', 'Customer\OrdersController@dayRent');
    Route::resource('orders', 'Customer\OrdersController');
    Route::get('orders/{id}/action/{action}', 'Customer\OrdersController@action');

    Route::get('bills', 'Customer\OrdersController@bills');
    Route::get('bills/{start}/{end}', 'Customer\OrdersController@queryBills');
});

Route::group(['middleware' => 'require_role:customer_service', 'prefix' => 'cs'], function() {
	Route::get('orders/state/{state}', 'CustomerService\OrdersController@orders');
    Route::get('orders/{id}/delete', 'CustomerService\OrdersController@destroy');
    Route::any('orders/add', 'CustomerService\OrdersController@add');
    Route::any('orders/add_airport/{customer_id}', 'CustomerService\OrdersController@toAirport');
    Route::any('orders/add_dayrent/{order_type}/{customer_id}', 'CustomerService\OrdersController@dayRent');
    Route::resource('orders', 'CustomerService\OrdersController');
    Route::any('orders/{id}/assign', 'CustomerService\OrdersController@assign');
    Route::any('orders/{id}/add_charge', 'CustomerService\OrdersController@addCharge');
    Route::any('orders/{id}/complain', 'CustomerService\OrdersController@complain');
    Route::any('orders/{id}/cfm_car', 'CustomerService\OrdersController@comfirmCar');

    Route::any('bills_select', 'CustomerService\OrdersController@billsSelect');
    Route::get('bills/{type}/{id}/{start}/{end}', 'CustomerService\OrdersController@queryBills');
    Route::get('bills/{type}/{id}', 'CustomerService\OrdersController@bills');

    Route::get('customers/deleted', 'CustomerService\CustomersController@deleted');
    Route::get('customers/{id}/delete', 'CustomerService\CustomersController@destroy');
    Route::resource('customers', 'CustomerService\CustomersController');

    Route::get('cars/deleted', 'CustomerService\CarsController@deleted');
    Route::get('cars/{id}/delete', 'CustomerService\CarsController@destroy');
    Route::resource('cars', 'CustomerService\CarsController');

    Route::get('car_company_resources/{id}', 'CustomerService\CarCompaniesController@resources');
    Route::get('car_companies/deleted', 'CustomerService\CarCompaniesController@deleted');
    Route::get('car_companies/{id}/delete', 'CustomerService\CarCompaniesController@destroy');
    Route::resource('car_companies', 'CustomerService\CarCompaniesController');

    Route::get('drivers/deleted', 'CustomerService\DriversController@deleted');
    Route::get('drivers/{id}/delete', 'CustomerService\DriversController@destroy');
    Route::resource('drivers', 'CustomerService\DriversController');

    Route::get('customer_services/deleted', 'CustomerService\CustomerServicesController@deleted');
    Route::get('customer_services/{id}/delete', 'CustomerService\CustomerServicesController@destroy');
    Route::resource('customer_services', 'CustomerService\CustomerServicesController');

    Route::get('logs', 'CustomerService\OpLogsController@index');

    Route::get('set_wx_menu', 'WeChat\CallbackController@setWxMenu');
});

Route::group(['middleware' => 'require_role:car_company', 'prefix' => 'cc'], function() {
    Route::get('bills', 'CarCompany\BillsController@bills');
    Route::get('bills/{start}/{end}', 'CarCompany\BillsController@queryBills');
});

Route::group(['middleware' => 'require_role:all'], function() {
    Route::post('upload/{type}', 'UploadController@upload');

    Route::get('info/cars/{id}', 'InfoController@carInfo');
    Route::get('info/drivers/{id}', 'InfoController@driverInfo');
});


Route::group(['prefix' => 'wx'], function() {
    Route::any('callback', 'WeChat\CallbackController@serve');

    Route::get('set', function() {
        \Session::put('wx_openid', 'oEAffvwsrzrgrcTrktcIpY6TgWmg');
        return 'oEAffvwsrzrgrcTrktcIpY6TgWmg';
    });

    Route::any('driver/bind', 'Driver\WxController@bind');
    Route::group(['middleware' => 'require_wx_role:driver', 'prefix' => 'driver'], function() {
        Route::get('test', function() {
            var_dump(\Session::get('wx_openid'));
            var_dump(\Auth::user());
            // \Session::put('wx_openid', 'ojqAWwwE_0qeBlMlz-rkN9nrFefM');
            return 'test';
        });

        Route::resource('orders', 'Driver\WxOrdersController');
        Route::any('orders/{id}/sign', 'Driver\WxOrdersController@signature');
        Route::any('orders/{id}/add_charge', 'Driver\WxOrdersController@addCharge');
        Route::any('orders/{id}/add_info', 'Driver\WxOrdersController@addInfo');
        Route::any('orders/{id}/add_child', 'Driver\WxOrdersController@addChild');
        Route::get('orders/{id}/complete_parent', 'Driver\WxOrdersController@completeParent');
    });
});