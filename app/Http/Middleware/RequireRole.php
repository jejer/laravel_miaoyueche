<?php

namespace App\Http\Middleware;

use Closure;

class RequireRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $request->user();

        if ($role == 'all' and $user) {
            return $next($request);
        }

        if (!$user or !$user->hasRole($role)) {
            \Auth::logout();
            return redirect('auth/login');
        }
        
        return $next($request);
    }
}
