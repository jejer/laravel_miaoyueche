<?php

namespace App\Http\Middleware;

use Auth;
use Wechat;

use Closure;

class RequireWxRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (!$request->session()->has('wx_openid')) {
            $wx_user = Wechat::auth()->authorize(null, 'snsapi_base', 'STATE');
            $request->session()->put('wx_openid', $wx_user['openid']);
        }

        $wx_openid = $request->session()->get('wx_openid');
        $user = \App\User::where('wx_openid', $wx_openid)->first();
        if (!$user or !$user->hasRole($role)) {
            abort(404);
        }

        // 不能在middleware中使用Auth::login($user)！可能是laravel的bug。估计和session设置时间有冲突！
        // Auth::login($user);
        return $next($request);
    }
}
