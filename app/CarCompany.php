<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarCompany extends Model
{
    use SoftDeletes;

    /**
     * Protect some field from mass assignment
     * @var array
     */
    protected $fillable = ['name', 'contact', 'contact_title', 'contact_phone', 'contact_email', 'note'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function cars() {
        return $this->hasMany('App\Car');
    }
}
