<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpLog extends Model
{
	protected $fillable = ['detail'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
