<?php

namespace App\Listeners;

use Log;
use App\Events\OrderCompletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Wechat;

use App\Http\ThirdParties\YunTongXun\SDK as SMS;

class OrderCompletedEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SMS $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCompletedEvent  $event
     * @return void
     */
    public function handle(OrderCompletedEvent $event)
    {
        $order = $event->order;

        Log::info('new order completed handler: order_id = '.$order->id);

        $data = array(
            "first"    => "您的订单已完成",
            "keyword1" => $order->id,
            "keyword2" => $order->time_final,
        );
        Log::info('new order completed handler: order_id = '.$order->time_final);
        Wechat::notice()->send($order->driver->user->wx_openid, 
                               'sjTPIgSOHD09aMSON4JZN5f51ii78EMCDxCDeDnSvsM',
                               $data,
                               url('/wx/driver/orders', $order->id));
    }
}
