<?php

namespace App\Listeners;

use Log;
use App\Events\OrderCarComfirmedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Wechat;

use App\Http\ThirdParties\YunTongXun\SDK as SMS;

class OrderCarComfirmedEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SMS $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCarComfirmedEvent  $event
     * @return void
     */
    public function handle(OrderCarComfirmedEvent $event)
    {
        $order = $event->order;

        Log::info('new order car comfirmed handler: order_id = '.$order->id);

        $mobile = $order->driver->mobile;
        $data = [$order->id];

        $result = $this->sms->sendTemplateSMS($mobile, $data, 35899);

        Log::info(json_encode($result));


        $data = array(
            "first"    => "您有一条新订单!",
            "tradeDateTime" => $order->time_use,
            "orderType" => $order->order_type()->name,
            "customerInfo" => $order->customer->name,
            "orderItemName" => "车型",
            "orderItemData" => $order->car_type()->name,
        );
        Wechat::notice()->send($order->driver->user->wx_openid, 
                               'emGkVhPqsBjNE8i70E-EygayFn2o_TAvcHxDkmPNvXk',
                               $data,
                               url('/wx/driver/orders', $order->id));
    }
}
