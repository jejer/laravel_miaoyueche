<?php

namespace App\Listeners;

use Log;
use App\Events\NewOrderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\ThirdParties\YunTongXun\SDK as SMS;

class NewOrderEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SMS $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  NewOrderEvent  $event
     * @return void
     */
    public function handle(NewOrderEvent $event)
    {
        Log::info('new order event handler: order_id = '.$event->order->id);

        $mobiles = '';

        $users = \App\Role::ofName('customer_service')->firstOrFail()->users;
        foreach ($users as $user) {
            if ($user->userinfo->accept_sms) {
                $mobiles = $mobiles.$user->userinfo->mobile.',';
            }
        }

        $mobiles = substr($mobiles, 0, -1);
        Log::info($mobiles);

        $result = $this->sms->sendTemplateSMS($mobiles, [$event->order->id], 35898);

        Log::info(json_encode($result));
    }
}
