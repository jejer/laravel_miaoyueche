<?php

namespace App\Listeners;

use Log;
use App\Events\OrderCarAssignedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\ThirdParties\YunTongXun\SDK as SMS;

class OrderCarAssignedEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SMS $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCarAssignedEvent  $event
     * @return void
     */
    public function handle(OrderCarAssignedEvent $event)
    {
        Log::info('new order car assigned handler: order_id = '.$event->order->id);

        $mobile = $event->order->mobile;
        $data = [$event->order->id,
                 $event->order->driver->name,
                 $event->order->driver->mobile,
                 $event->order->car->lpn,
                 $event->order->car->model];

        $result = $this->sms->sendTemplateSMS($mobile, $data, 39453);

        Log::info(json_encode($result));
    }
}
