<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('cu.sidebar', function ($view) {
            $view->with('company', Auth::user()->customer->name);
            $view->with('contact', Auth::user()->customer->contact);
            $view->with('order_count', Auth::user()->customer->orders()->count());
            $view->with('balance', Auth::user()->customer->balance);
            $view->with('point', Auth::user()->customer->point);
        });

        view()->composer('cs.sidebar', function ($view) {
            $view->with('contact', Auth::user()->name);
            $view->with('order_count_waiting', \App\Order::ofState('waiting')->withoutChild()->count());
            $view->with('order_count_inprogress', \App\Order::ofState('inprogress')->withoutChild()->count());
            $view->with('order_count_completed', \App\Order::ofState('completed')->withoutChild()->count());
            $view->with('order_count_all', \App\Order::withoutChild()->count());
            $view->with('order_count_deleted', \App\Order::onlyTrashed()->withoutChild()->count());
        });

        view()->composer('cc.sidebar', function ($view) {
            $view->with('contact', Auth::user()->name);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
