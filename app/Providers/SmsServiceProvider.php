<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

use App\Http\ThirdParties\YunTongXun\SDK;

class SmsServiceProvider extends ServiceProvider
{
    // protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
            $this->app->singleton('App\Http\ThirdParties\YunTongXun\SDK', function ($app) {
            //主帐号,对应开官网发者主账号下的 ACCOUNT SID
            $accountSid = '8a48b5514f4fc588014f6801acb73067';
            
            //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
            $accountToken = '725f8e412af94661b28c2f40e674d942';
            
            //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
            //在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
            $appId = 'aaf98f894fb19cec014fb545250a01ec';
            
            //请求地址
            //沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
            //生产环境（用户应用上线使用）：app.cloopen.com
            $serverIP = 'app.cloopen.com';
            
            //请求端口，生产环境和沙盒环境一致
            $serverPort = '8883';
            
            //REST版本号，在官网文档REST介绍中获得。
            $softVersion = '2013-12-26';

            $sdk = new SDK($serverIP, $serverPort, $softVersion);
            $sdk->setAccount($accountSid, $accountToken);
            $sdk->setAppId($appId);

            return $sdk;
        });
    }
}
