<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Protect some field from mass assignment
     * @var array
     */
    protected $fillable = ['name', 'contact', 'contact_title', 'contact_phone', 'contact_email', 'note'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }
}
