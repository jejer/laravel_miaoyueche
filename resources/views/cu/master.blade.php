@extends('master')

@section('body')
<body>
    <div class="righter nav-navicon" id="admin-nav">
        <div class="mainer">
            <div class="admin-navbar">
                <span class="float-right">
                    <a class="button button-little bg-yellow" href="{{ url('/auth/logout') }}">注销登录</a>
                </span>
{{--                 <span class="float-right">
                    <a class="button button-little bg-yellow" href="{{ url('cu/userinfo') }}">设置</a>
                </span> --}}
                <ul class="nav nav-inline admin-nav">
                    <li> <img  src="/images/logo.png" class="logo"/></li>
                    <li @if($parent_page == 'new_order')class="active"@endif><a href="{{ url('/cu/orders/create/order_type_select') }}" class="icon-home right5">预约用车</a>
                </li>
            </li>
            <li @if($parent_page == 'orders')class="active"@endif><a href="{{ url('/cu/orders') }}" class="icon-shopping-cart right5">订单管理</a></li>
            <li @if($parent_page == 'bills')class="active"@endif><a href="{{ url('/cu/bills') }}" class="icon-user right5">对账单</a></li>
        </ul>
    </div>
    <div class="admin-bread">
        <ul class="bread">
            <li><a href="#" class="icon-home">开始</a></li>
            <li>{{ $page_name or '' }}</li>
        </ul>
    </div>
</div>
</div>
<div class="admin">
<div class="line-big">

    @include('cu.sidebar')

    <div class="xm10">
        @if(Session::has('success'))
        <div class="alert-box success">
            <h2>{{ Session::get('success') }}</h2>
        </div>
        @endif

        @yield('main')
    </div>
</body>
@endsection

