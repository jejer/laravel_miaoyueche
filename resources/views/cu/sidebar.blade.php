    <div class="xm2">
        <div class="panel border-back">
            <div class="panel-body text-center ">
                {{ $company }} {{ $contact }}
            </div>
            <div class="panel-foot bg-back border-back">您好.</div>
        </div>
        <br />
        <div class="panel">
            <div class="panel-head"><strong>站点统计</strong></div>
            <ul class="list-group">
                <li><span class="float-right badge bg-main">{{$order_count}}</span><span class="icon-shopping-cart"></span> 订单</li>
                <li><span class="float-right badge bg-main">{{$balance}}</span><span class="icon-database"></span>储值余额</li>
                <li><span class="float-right badge bg-main">{{$point}}</span><span class="icon-database"></span>积分</li>
            </ul>
        </div>
        <br />
    </div>