{? $parent_page = "new_order" ?}

@extends('cu.master')

@section('page_name', '预约用车 - 接送机')

@section('head')
<script src="/js/pintuer.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // <!--接送机的变化-->
        $(".jieji").click(function () {
            $('#jiesongji').val("jieji");
        })
        $(".songji").click(function () {
            $('#jiesongji').val("songji");
        })

        // <!--价格的变化-->
        $(".car_type").change(function() {
            switch (this.value) {
                case "1": //jingji
                $('#price').text('160');
                break;

                case "2": //shushi
                $('#price').text('180');
                break;

                case "3": //shangwu
                $('#price').text('200');
                break;

                case "4": //suv
                $('#price').text('200');
                break;

                case "5": //shehua
                $('#price').text('320');
                break;

                case "6": //zhongba
                $('#price').text('360');
                break;
            }
        })
    
        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
        });

    })
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>接送机</strong></div>
    <div class="panel-body">
        <div class="alert alert-red whitea"><span class="close"></span><strong>注意：</strong>一些注意的信息，解释条款</div>
        {!! Form::model($order, ['method'=>'patch', 'url'=>'/cu/orders/'.$order->id, 'class'=>'form-x']) !!}
            @include('include.form_2airport')
        {!! Form::close() !!}
    </div>
</div>
@endsection