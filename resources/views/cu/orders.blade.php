{? $page_name = "订单列表" ?}
{? $parent_page = "orders" ?}

@extends('cu.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('.pagination').bootpag({
			total: {{ $orders->lastPage() }},
			page: {{ $orders->currentPage() }},
			maxVisible: 10
		}).on('page', function(event, num){
			var url = "{{ $orders->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
			window.location.href = url;
		});
	})
</script>
@endsection

@section('main')
<div class="panel admin-panel">
	<div class="panel-head"><strong>{{ $page_name }}</strong></div>
	<table class="table table-hover">
		<tr>
			<th width="120">订单号</th>
			<th width="120">分类</th>
			<th width="100">用车时间</th>
			<th width="60">状态</th>
			<th width="100">操作</th>
		</tr>
		@foreach ($orders as $order)
		<tr>
			<td>{{ $order->id }}</td>
			<td>{{ $order->order_type()->name }}</td>
			<td>{{ $order->time_use }}</td>
			<td>{{ $order->order_state()->name }}</td>
			<td><a class="button border-blue button-little" href="{{ url('/cu/orders', $order->id) }}">查看</a></td>
		</tr>
		@endforeach
	</table>
	<div class="panel-foot text-center">
	<p class="pagination"></p>
	</div>
</div>
@endsection