{? $page_name = "订单详情" ?}
{? $parent_page = "orders" ?}

@extends('cu.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($order, ['class'=>'form-x']) !!}

            @include('include.order_review')

            <div class="form-button">
                @if ($order->order_state_id == 'w_cus_cfm_car')
                <a class="button" href="{{ url('/cu/orders/'.$order->id.'/action', 'cfm_car') }}">确认用车</a>
                @endif
            </div>
        {!! Form::close() !!}
    </div>
</div>

{? $review_child_url = '/cu/orders' ?}
@include('include.order_review_child_orders')

@include('include.order_review_histories')

@include('include.order_review_charges')

<input type="button" onClick="window.print()" value="打印"/>

@endsection