{? $page_name = "司机信息" ?}
{? $parent_page = "orders" ?}


@extends('cs.master');


@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($driver, ['class'=>'form-x']) !!}

            @include('include.order_review_driver_info')

        {!! Form::close() !!}
    </div>
</div>
@endsection