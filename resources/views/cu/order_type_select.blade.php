{? $parent_page = "new_order" ?}

@extends('cu.master')

@section('main')
<div class="panel">
	<div class="panel-head"><strong>预约用车</strong></div>
	<table class="btnThree ">
		<tr>
			<td><a href="{{ url('/cu/orders/create/to_airport') }}">
				<img src="/images/jieji.png" /><h3>接/送机</h3>
			</a></td>
			<td><a href="{{ url('/cu/orders/create/day_rent/halfday') }}">
			<img src="/images/banri.png" /><h3>半日租</h3></a></td>
			<td><a href="{{ url('/cu/orders/create/day_rent/fullday') }}">
			<img src="/images/ri.png" /><h3>日租</h3></a></td>
		</tr>
	</table>
</div>
@endsection