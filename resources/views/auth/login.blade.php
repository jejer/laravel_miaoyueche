<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>妙约车 登录</title>
    <link rel="stylesheet" href="/css/pintuer.css">
    <link rel="stylesheet" href="/css/admin.css">
    <script src="/js/jquery.js"></script>
    <!--     <script src="/js/pintuer.js"></script>
    <script src="/js/respond.js"></script> -->
    <script src="/js/admin.js"></script>
    <!--     <link type="image/x-icon" href="http://www.pintuer.com/favicon.ico" rel="shortcut icon" />
    <link href="http://www.pintuer.com/favicon.ico" rel="bookmark icon" /> -->



</head>

<body style="background-color: #fcfdfd">
    <div style="padding-top: 50px; padding-bottom: 50px; background-color: #5a669c">
        <div class="container">
            <div class="line">
                <div class="xs2 "></div>
                <div class="xs3 ">
                    <br />
                    <br />
                    <br />
                    <br />
                    <img src="/images/logo.png" width="200px" alt="" /></a>
                </div>
                <div class="xs4 ">
                    {!! Form::open() !!}

                        <div class="panel">
                            <div class="panel-head"><strong>登录</strong></div>
                            <div class="panel-body" style="padding: 30px;">
                                <div class="form-group">
                                    <div class="field field-icon-right">
                                        <input type="text" class="input" name="email" placeholder="邮箱" data-validate="required:请填邮箱地址,length#>=3:邮箱地址长度不符合要求" />
                                        <span class="icon icon-user"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="field field-icon-right">
                                        <input type="password" class="input" name="password" placeholder="登录密码" data-validate="required:请填写密码,length#>=3:密码长度不符合要求" />
                                        <span class="icon icon-key"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="field">
                                        <input type="text" class="input" name="captcha" placeholder="填写右侧的验证码" data-validate="required:请填写右侧的验证码" />
                                        {{-- {!! captcha_img() !!} --}}
                                        <img src="{!! Captcha::src('flat') !!}" width="80" height="32" class="passcode" />
                                    </div>
                                </div>
                            </div>
                            <div class="panel-foot text-center">
                                <button class="button button-block bg-main text-big">立即登录</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                </div>
            </div>
        </div>
    </div>
</body>


</html>