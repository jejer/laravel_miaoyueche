{? $page_name = "填写信息".$order->id ?}

@extends('wx_master')

@section('head')

<script type="text/javascript">
    $(document).ready(function () {
        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
            // minDateTime: "{date("Y-m-d")} 00:00:00",
            // minDate: "{date("Y-m-d")}"
        });

    })
</script>
@endsection

@section('body')
<div class="row" style="margin: 10px 0px">
    @if ($form == 'start_info')
        @include('include.wx.start_info')
    @elseif ($form == 'end_info')
        @include('include.wx.end_info')
    @endif
</div>
@endsection