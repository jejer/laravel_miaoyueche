{? $page_name = "我的订单".$order->id ?}

@extends('wx_master')

@section('head')
<!--签名-->
<link href="/css/jquery.signaturepad.css" rel="stylesheet">
{{-- <script src="/js/jquery-1.7.2.min.js"></script> --}}
<script src="/js/jquery.signaturepad.min.js"></script>
<script>
$(document).ready(function() {
    $('.sigPad').signaturePad( { drawOnly:true, onDrawEnd:function(){
        $('.output2').attr("value", $('.sigPad').signaturePad().getSignatureImage());
    }});
    $('canvas.pad').attr('width', $(window).width());
});
</script>
<style type="text/css">
.sigPad {
  margin: 0;
  padding: 0;
  width: 100%; /* Change the width */
}
.sigWrapper {
  clear: both;
  height: 200px; /* Change the height */
  border: 1px solid #ccc;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
            // minDateTime: "{date("Y-m-d")} 00:00:00",
            // minDate: "{date("Y-m-d")}"
        });

    })
</script>
@endsection

@section('body')
<div class="row" style="margin: 10px 0px">
    {!! Form::model($order) !!}
    
        @include('include.wx.order_review')

    {!! Form::close() !!}

    @if ($form == 'final_sign')
        {? $sign_button_text = "签名确认订单完成" ?}
        @include('include.wx.sign')
    @elseif ($form == 'pickup_sign')
        {? $sign_button_text = "签名确认订单起始信息" ?}
        @include('include.wx.sign')
    @elseif ($form == 'start_info')
        @include('include.wx.start_info')
    @elseif ($form == 'end_info')
        @include('include.wx.end_info')
    @elseif ($form == 'add_child')
        @include('include.wx.add_child')
    @endif
    
    <br/>
    @foreach ($buttons as $text => $url)
    <a href='{{ $url }}' class="btn btn-block btn-default btn-primary">{{ $text }}</a>
    @endforeach

</div>

{? $review_child_url = '/wx/driver/orders' ?}
@include('include.wx.order_review_child_orders')

@include('include.wx.order_review_charges')

@endsection