{? $page_name = "最新订单" ?}

@extends('wx_master')

@section('body')
<div class="row" style="margin: 10px 0px">
    <div class="col-xs-12">
        @foreach ($orders as $order)
        <a href="{{ url('/wx/driver/orders', $order->id) }}" class="panel panel-info ">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fr small">用车时间 {{ $order->time_use }}</span>NO:{{ $order->id }}</h3>
            </div>
            <div class="panel-body">
                {{ $order->customer->name }} {{ $order->location_start }}
            </div>
            <div class="panel-footer"><span class="fr small">{{ $order->order_state()->name }}</span>{{ $order->order_type()->name }}</div>
        </a>
        @endforeach
    </div>
</div>
@endsection