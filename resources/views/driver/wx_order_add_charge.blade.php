{? $page_name = "添加附加收费".$order->id ?}

@extends('wx_master')

@section('body')
<div class="row" style="margin: 10px 0px">
    {!! Form::open() !!}
        <div class="form-group">
            {!! Form::label('name','收费项目:') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price','价格:') !!}
            {!! Form::input('number','price',null,['class'=>'form-control','step'=>'0.01']) !!}
        </div>
        {!! Form::submit("提交", ['class'=>'btn btn-block btn-default btn-primary']) !!}
    {!! Form::close() !!}

</div>
@endsection