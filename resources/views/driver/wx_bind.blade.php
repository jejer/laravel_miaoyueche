{? $page_name = "司机账号绑定" ?}

@extends('wx_master')

@section('body')
<div class="row" style="margin: 10px 0px">
    <div class="col-xs-12">
        {!! Form::open() !!}
            <div class="form-group">
                {!! Form::label('chinese','姓名:') !!}
                {!! Form::text('chinese',null,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('id_number','身份证号码:') !!}
                {!! Form::text('id_number',null,['class'=>'form-control']) !!}
            </div>
            {!! Form::submit('绑定',['class'=>'btn btn-block btn-default btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection