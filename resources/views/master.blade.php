<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="renderer" content="webkit">
	<title>妙约车 @yield('title')</title>
	<link rel="stylesheet" href="/css/pintuer.css">
	<link rel="stylesheet" href="/css/admin.css">
	<script src="/js/jquery.js"></script>
<!--     <script src="/js/pintuer.js"></script>
	<script src="/js/respond.js"></script> -->
	<script src="/js/admin.js"></script>
<!--     <link type="image/x-icon" href="http://www.pintuer.com/favicon.ico" rel="shortcut icon" />
	<link href="http://www.pintuer.com/favicon.ico" rel="bookmark icon" /> -->

	<!--时间日期选择器-->
	<link rel="stylesheet" type="text/css" href="/css/DateTimePicker.css" />
	<script type="text/javascript" src="/js/DateTimePicker.js"></script>
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="/css/DateTimePicker-ltie9.css" />
		<script type="text/javascript" src="/js/DateTimePicker-ltie9.js"></script>
	<![endif]-->
 
	<!-- <script src="/laydate/laydate.js"></script> -->
 <!--   <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
	<script src="/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/bootstrap.min.css">-->
	
	@yield('head')
</head>

@yield('body')

</html>
