<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no">
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>

    <!--时间日期选择器-->
    <link rel="stylesheet" type="text/css" href="/css/DateTimePicker.css" />
    <script type="text/javascript" src="/js/DateTimePicker.js"></script>
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/css/DateTimePicker-ltie9.css" />
        <script type="text/javascript" src="/js/DateTimePicker-ltie9.js"></script>
    <![endif]-->

    <script type="text/javascript">
        $(function () {

        })
    </script>
    <style>
        .form-control {
            height: auto;
            min-height: 30px;
            color: #808080;
            font-weight: normal;
        }
    </style>
    @yield('head')
</head>
<body>
    <div class=" bg-primary" style="overflow: hidden">
        <div style="margin: 10px">
            izcar 平台 {{ $page_name or '' }}
        </div>
    </div>
    @yield('body')
</body>
</html>
