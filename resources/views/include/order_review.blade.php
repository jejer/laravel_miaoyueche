            <div class="form-group">
                <div class="label">
                    {!! Form::label('created_at','客户公司:') !!}
                </div>
                <div class="field">
                    {!! Form::text('created_at', $order->customer->name,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('created_at','订单添加时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('created_at',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('is_complain','订单是否在投诉状态:') !!}
                </div>
                <div class="field">
                    {!! Form::text('is_complain',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('department','部门:') !!}
                </div>
                <div class="field">
                    {!! Form::text('department',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact','联系人:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('mobile','电话号码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mobile',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('order_type_id','订单类型:') !!}
                </div>
                <div class="field">
                    {!! Form::text('order_type_id',$order->order_type()->name.($order->parent_order_id?' 子订单':''),['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_type_id','用车类型:') !!}
                </div>
                <div class="field">
                    {!! Form::text('car_type_id',$order->car_type()->name,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('time_use','用车时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('time_use',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('order_state','订单状态:') !!}
                </div>
                <div class="field">
                    {!! Form::text('order_state',$order->order_state()->name,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>

            @if ($order->order_type_id == 'airport')
            <div class="form-group">
                <div class="label">
                    {!! Form::label('is_pickup','接送机:') !!}
                </div>
                <div class="field">
                    @if ($order->is_pickup == '1')
                    {!! Form::text('is_pickup','接机',['class'=>'input','disabled'=>True]) !!}
                    @else
                    {!! Form::text('is_pickup','送机',['class'=>'input','disabled'=>True]) !!}
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_start','地点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_start',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('flight_no','航班号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('flight_no',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>

            @else
            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_start','起点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_start',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_end','终点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_end',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('mile_start','起始里程:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mile_start',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('mile_end','终止里程:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mile_end',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('time_start','起始时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('time_start',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('time_end','终止时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('time_end',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            @endif

            <div class="form-group">
                <div class="label">
                    {!! Form::label('note','备注:') !!}
                </div>
                <div class="field">
                    {!! Form::text('note',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            
            @if ($order->car_id)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_lpn','车辆牌号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('car_lpn',$order->car->lpn,['class'=>'input','disabled'=>True]) !!}
                    <a href="{{ url('/info/cars', $order->car_id) }}" class="button button-small border-green">查看车辆信息</a>
                </div>
            </div>
            @endif
            @if ($order->driver_id)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('driver_name','司机:') !!}
                </div>
                <div class="field">
                    {!! Form::text('driver_name',$order->driver->name,['class'=>'input','disabled'=>True]) !!}
                    <a href="{{ url('/info/drivers', $order->driver_id) }}" class="button button-small border-green">查看司机信息</a>
                </div>
            </div>
            @endif

            @if ($order->sign_pickup)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('sign_pickup','初始信息确认签名:') !!}
                </div>
                <div class="field">
                    <img src="/uploads/signatures/{{ $order->sign_pickup }}"/>
                </div>
            </div>
            @endif
            @if ($order->sign_final)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('sign_final','订单完成确认签名:') !!}
                </div>
                <div class="field">
                    <img src="/uploads/signatures/{{ $order->sign_final }}"/>
                </div>
            </div>
            @endif