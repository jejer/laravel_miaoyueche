            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_company_id','所属公司:') !!}
                </div>
                <div class="field">
                    {!! Form::text('lpn',$car->car_company->name,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('lpn','车牌号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('lpn',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('model','车型:') !!}
                </div>
                <div class="field">
                    {!! Form::text('model',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('birth_year','年限:') !!}
                </div>
                <div class="field">
                    {!! Form::text('birth_year',null,['class'=>'input','disabled'=>True]) !!}
                </div>
            </div>

            @if ($car and $car->pic_license)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_license','行驶证照片:') !!}
                </div>
                <div class="field">
                    <img src="{{ url('/uploads/cars', $car->pic_license) }}"/>
                </div>
            </div>
            @endif

            @if ($car and $car->pic_insurance)
            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_insurance','保险照片:') !!}
                </div>
                <div class="field">
                    <img src="{{ url('/uploads/cars', $car->pic_insurance) }}"/>
                </div>
            </div>
            @endif