            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_company_id','所属公司:') !!}
                </div>
                <div class="field">
                    {!! Form::select('car_company_id',$car_companies,null,['class'=>'input']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('lpn','车牌号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('lpn',null,['class'=>'input','data-validate'=>'required:车牌号']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('model','车型:') !!}
                </div>
                <div class="field">
                    {!! Form::text('model',null,['class'=>'input','data-validate'=>'required:车型']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('birth_year','年限:') !!}
                </div>
                <div class="field">
                    {!! Form::text('birth_year',null,['class'=>'input','data-validate'=>'required:年限']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_license','行驶证照片:') !!}
                </div>
                <div class="field">
                    {!! Form::text('pic_license',null,['id'=>'pic_license','class'=>'input','readonly'=>True]) !!}
                    @if ($car and $car->pic_license)
                    <img src="{{ url('/uploads/cars', $car->pic_license) }}" width="400"/>
                    @endif
                    <div id="fine-uploader-gallery-pic_license"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_insurance','保险照片:') !!}
                </div>
                <div class="field">
                    {!! Form::text('pic_insurance',null,['id'=>'pic_insurance','class'=>'input','readonly'=>True]) !!}
                    @if ($car and $car->pic_insurance)
                    <img src="{{ url('/uploads/cars', $car->pic_insurance) }}" width="400"/>
                    @endif
                    <div id="fine-uploader-gallery-pic_insurance"></div>
                </div>
            </div>