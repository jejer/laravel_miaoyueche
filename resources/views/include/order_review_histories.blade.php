    <div class="panel admin-panel">
        <div class="panel-head"><strong>订单历史</strong></div>
        <table class="table table-hover">
            <tr>
                <th width="120">处理时间</th>
                <th width="120">处理信息</th>
                <th width="100">操作人</th>
            </tr>
            @foreach ($order->order_histories as $history)
            <tr>
                <td>{{ $history->created_at }}</td>
                <td>{{ $history->info }}</td>
                <td>{{ $history->operator }}</td>
            </tr>
            @endforeach
        </table>
    </div>