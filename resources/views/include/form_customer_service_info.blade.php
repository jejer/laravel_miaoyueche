            <div class="form-group">
                <div class="label">
                    {!! Form::label('name','姓名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('name',null,['class'=>'input','data-validate'=>'required:姓名']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('mobile','手机号码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mobile',null,['class'=>'input','data-validate'=>'required:手机号码']) !!}
                </div>
            </div>