            <div class="form-group">
                <div class="label">
                    {!! Form::label('name','司机姓名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('name',null,['class'=>'input','data-validate'=>'required:司机姓名']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('id_number','身份证号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('id_number',null,['class'=>'input','data-validate'=>'required:身份证号']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('mobile','手机号码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mobile',null,['class'=>'input','data-validate'=>'required:手机号码']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_id_1','身份证正面:') !!}
                </div>
                <div class="field">
                    {!! Form::text('pic_id_1',null,['id'=>'pic_id_1','class'=>'input','readonly'=>True]) !!}
                    @if ($driver and $driver->pic_id_1)
                    <img src="{{ url('/uploads/drivers', $driver->pic_id_1) }}" width="400"/>
                    @endif
                    <div id="fine-uploader-gallery-pic_id_1"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_id_2','身份证反面:') !!}
                </div>
                <div class="field">
                    {!! Form::text('pic_id_2',null,['id'=>'pic_id_2','class'=>'input','readonly'=>True]) !!}
                    @if ($driver and $driver->pic_id_2)
                    <img src="{{ url('/uploads/drivers', $driver->pic_id_2) }}" width="400"/>
                    @endif
                    <div id="fine-uploader-gallery-pic_id_2"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('pic_license','驾驶执照:') !!}
                </div>
                <div class="field">
                    {!! Form::text('pic_license',null,['id'=>'pic_license','class'=>'input','readonly'=>True]) !!}
                    @if ($driver and $driver->pic_license)
                    <img src="{{ url('/uploads/drivers', $driver->pic_license) }}" width="400"/>
                    @endif
                    <div id="fine-uploader-gallery-pic_license"></div>
                </div>
            </div>