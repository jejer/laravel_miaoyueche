            <div class="form-group">
                <div class="label">
                    {!! Form::label('name','公司名称:') !!}
                </div>
                <div class="field">
                    {!! Form::text('name',null,['class'=>'input','data-validate'=>'required:公司名称']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact','联系人:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact',null,['class'=>'input','data-validate'=>'required:联系人']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact_title','职务:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact_title',null,['class'=>'input','data-validate'=>'required:职务']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact_phone','联系方式:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact_phone',null,['class'=>'input','data-validate'=>'required:联系方式']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('balance','储值余额:') !!}
                </div>
                <div class="field">
                    {!! Form::input('number','balance',null,['class'=>'input','step'=>'0.01','data-validate'=>'required:储值余额']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('note','备注:') !!}
                </div>
                <div class="field">
                    {!! Form::textarea('note',null,['class'=>'input']) !!}
                </div>
            </div>