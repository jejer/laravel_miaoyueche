            <div class="form-group">
                <div class="label">
                    {!! Form::label('time_use','用车时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('time_use',null,['class'=>'input','data-validate'=>'required:用车时间','data-field'=>'datetime','readonly'=>True]) !!}
                    <div id="dtBox"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_start','起点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_start',null,['class'=>'input','data-validate'=>'required:起点']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_end','终点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_end',null,['class'=>'input','data-validate'=>'required:终点']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('department','部门:') !!}
                </div>
                <div class="field">
                    {!! Form::text('department',null,['class'=>'input','data-validate'=>'required:部门']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact','联系人:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact',null,['class'=>'input','data-validate'=>'required:联系人']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('mobile','联系电话:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mobile',null,['class'=>'input','data-validate'=>'required:联系电话']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_type_id','用车类型:') !!}
                </div>
                <div class="field">
                    <div style="margin-top: 10px;">
                        {!! Form::radio('car_type_id','eco',null,['id'=>'ct_eco']) !!}<span>经济&nbsp;&nbsp;</span>
                        {!! Form::radio('car_type_id','com',null,['id'=>'ct_com']) !!}<span>舒适&nbsp;&nbsp;</span>
                        {!! Form::radio('car_type_id','bus_7',null,['id'=>'ct_bus_7']) !!}<span>商务7座&nbsp;&nbsp;</span>
                        {!! Form::radio('car_type_id','suv',null,['id'=>'ct_suv']) !!}<span>SUV&nbsp;&nbsp;</span>
                        {!! Form::radio('car_type_id','lux',null,['id'=>'ct_lux']) !!}<span>豪华&nbsp;&nbsp;</span>
                        {!! Form::radio('car_type_id','cmb_14_55',null,['id'=>'cmb_14_55']) !!}<span>14-55座中巴&nbsp;&nbsp;</span>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="label">
                    {!! Form::label('note','备注:') !!}
                </div>
                <div class="field">
                    {!! Form::textarea('note',null,['class'=>'input']) !!}
                </div>
            </div>