    {!! Form::model($order, ['url'=>url('/wx/driver/orders', $order->id).'/sign','class'=>'sigPad']) !!}

        <p class="drawItDesc">在此签名</p>
        <ul class="sigNav">
            <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
            <li class="clearButton"><a href="#clear">擦除</a></li>
        </ul>
        <div class="sig sigWrapper">
            <div class="typed"></div>
            <canvas class="pad" height="200"></canvas>
            <input type="hidden" name="output" class="output">
            <input type="hidden" name="sig2" class="output2">
        </div>

        {!! Form::submit($sign_button_text, ['class'=>'btn btn-block btn-default btn-primary']) !!}
    {!! Form::close() !!}