    {!! Form::open(['url'=>url('/wx/driver/orders', $order->id).'/add_info']) !!}

        <div class="form-group">
            {!! Form::label('time_end','终止时间:', ['style'=>'color:Red']) !!}
            {!! Form::text('time_end',null,['class'=>'form-control', 'data-field'=>'datetime', 'readonly'=>True]) !!}
            <div id="dtBox"></div>
        </div>
        <div class="form-group">
            {!! Form::label('mile_end','终止公里数:', ['style'=>'color:Red']) !!}
            {!! Form::text('mile_end',null,['class'=>'form-control']) !!}
        </div>

        {!! Form::submit("提交中止信息", ['class'=>'btn btn-block btn-default btn-primary']) !!}
    {!! Form::close() !!}