    @if ($order->child_orders()->count())
    <div class="panel admin-panel">
        <div class="panel-head"><strong>子订单</strong></div>
        <table class="table table-hover">
            <tr>
                <th width="120">用车日期</th>
                <th width="60">详细</th>
            </tr>
            @foreach ($order->child_orders as $child)
            <tr>
                <td>{{ \Carbon\Carbon::parse($child->getOriginal('time_use'))->format('Y-m-d') }}</td>
                <td><a href="{{ url($review_child_url, $child->id) }}" class="panel panel-info ">查看</a></td>
            </tr>
            @endforeach
        </table>
    </div>
    @endif