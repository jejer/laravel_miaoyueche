    <div class="panel admin-panel">
        <div class="panel-head"><strong>费用</strong></div>
        <table class="table table-hover">
            <tr>
                <th width="120">发生时间</th>
                <th width="120">费用名</th>
                <th width="100">价格</th>
                <th width="100">数量</th>
                <th width="100">总价</th>
            </tr>
            {? $total_price = 0.0 ?}
            @foreach ($order->order_charges as $charge)
            <tr>
                <td>{{ $charge->created_at }}</td>
                <td>{{ $charge->name }}</td>
                <td>{{ $charge->price }}</td>
                <td>{{ $charge->count }} {{ $charge->count_unit }}</td>
                <td>{{ $charge->price * $charge->count }}</td>
                {? $total_price += $charge->price * $charge->count ?}
            </tr>
            @endforeach
        </table>
        <div class="field">
            @if ($order->order_state_id == 'finished')
            <span class="text-red " style="font-size: 25px;">合计 {{ $order->price_final }}</span>
            @else
            <span class="text-red " style="font-size: 25px;">当前费用合计 {{ $total_price }}</span>
            @endif
        </div>
    </div>