    {!! Form::open(['url'=>url('/wx/driver/orders', $order->id).'/add_child']) !!}

        <div class="form-group">
            {!! Form::label('child_date','子订单日期:', ['style'=>'color:Red']) !!}
            {!! Form::text('child_date',\Carbon\Carbon::now()->format('Y-m-d'),['class'=>'form-control', 'data-field'=>'date', 'readonly'=>True]) !!}
            <div id="dtBox"></div>
        </div>

        {!! Form::submit("添加子订单", ['class'=>'btn btn-block btn-default btn-primary']) !!}
    {!! Form::close() !!}