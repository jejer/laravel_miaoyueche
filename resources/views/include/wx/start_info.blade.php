    {!! Form::open(['url'=>url('/wx/driver/orders', $order->id).'/add_info']) !!}

        <div class="form-group">
            {!! Form::label('time_start','起始时间:', ['style'=>'color:Red']) !!}
            {!! Form::text('time_start',null,['class'=>'form-control', 'data-field'=>'datetime', 'readonly'=>True]) !!}
            <div id="dtBox"></div>
        </div>
        <div class="form-group">
            {!! Form::label('mile_start','起始公里数:', ['style'=>'color:Red']) !!}
            {!! Form::text('mile_start',null,['class'=>'form-control']) !!}
        </div>

        {!! Form::submit("提交初始信息", ['class'=>'btn btn-block btn-default btn-primary']) !!}
    {!! Form::close() !!}