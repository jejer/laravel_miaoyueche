        <div class="form-group">
            {!! Form::label('created_at','客户公司:') !!}
            {!! Form::text('created_at', $order->customer->name,['class'=>'form-control','disabled'=>True]) !!}
        <div class="form-group">
            {!! Form::label('created_at','订单添加时间:') !!}
            {!! Form::text('created_at',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('is_complain','订单是否在投诉状态:') !!}
            {!! Form::text('is_complain',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('department','部门:') !!}
            {!! Form::text('department',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('contact','联系人:') !!}
            {!! Form::text('contact',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('mobile','电话号码:') !!}
            {!! Form::text('mobile',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('order_type_id','订单类型:') !!}
            {!! Form::text('order_type_id',$order->order_type()->name.($order->parent_order_id?' 子订单':''),['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('car_type_id','用车类型:') !!}
            {!! Form::text('car_type_id',$order->car_type()->name,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('time_use','用车时间:') !!}
            {!! Form::text('time_use',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('order_state','订单状态:') !!}
            {!! Form::text('order_state',$order->order_state()->name,['class'=>'form-control','disabled'=>True]) !!}
        </div>

        @if ($order->order_type_id == 'airport')
        <div class="form-group">
            {!! Form::label('is_pickup','接送机:') !!}
            @if ($order->is_pickup == '1')
            {!! Form::text('is_pickup','接机',['class'=>'form-control','disabled'=>True]) !!}
            @else
            {!! Form::text('is_pickup','送机',['class'=>'form-control','disabled'=>True]) !!}
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('location_start','地点:') !!}
            {!! Form::text('location_start',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('flight_no','航班号:') !!}
            {!! Form::text('flight_no',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>

        @else
        <div class="form-group">
            {!! Form::label('location_start','起点:') !!}
            {!! Form::text('location_start',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('location_end','终点:') !!}
            {!! Form::text('location_end',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('mile_start','起始里程:') !!}
            {!! Form::text('mile_start',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('mile_end','终止里程:') !!}
            {!! Form::text('mile_end',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('time_start','起始时间:') !!}
            {!! Form::text('time_start',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('time_end','终止时间:') !!}
            {!! Form::text('time_end',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>
        @endif

        <div class="form-group">
            {!! Form::label('note','备注:') !!}
            {!! Form::text('note',null,['class'=>'form-control','disabled'=>True]) !!}
        </div>