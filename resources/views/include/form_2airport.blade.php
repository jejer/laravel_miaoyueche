            <div class="form-group">
                <div class="label">
                    {!! Form::label('is_pickup','接送机:') !!}
                </div>
                <div class="field">
                    <div class="button-group button-group-small radio">
                        <label class=@if($order->is_pickup=='1')"button active"@else"button"@endif">
                            {!! Form::radio('is_pickup','1',null) !!}<span class="icon icon-check"></span>接机
                        </label>
                        <label class=@if($order->is_pickup=='0')"button active"@else"button"@endif>
                            {!! Form::radio('is_pickup','0',null) !!}<span class="icon icon-check"></span>送机
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('location_start','地点:') !!}
                </div>
                <div class="field">
                    {!! Form::text('location_start',null,['class'=>'input','data-validate'=>'required:地点']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('department','部门:') !!}
                </div>
                <div class="field">
                    {!! Form::text('department',null,['class'=>'input','data-validate'=>'required:部门']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('contact','联系人:') !!}
                </div>
                <div class="field">
                    {!! Form::text('contact',null,['class'=>'input','data-validate'=>'required:联系人']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('mobile','联系电话:') !!}
                </div>
                <div class="field">
                    {!! Form::text('mobile',null,['class'=>'input','data-validate'=>'required:联系电话']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('time_use','用车时间:') !!}
                </div>
                <div class="field">
                    {!! Form::text('time_use',null,['class'=>'input','data-validate'=>'required:用车时间','data-field'=>'datetime','readonly'=>True]) !!}
                    <div id="dtBox"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_type_id','用车类型:') !!}
                </div>
                <div class="field">
                    <div style="margin-top: 10px;">
                        {!! Form::radio('car_type_id','eco',null,['id'=>'ct_eco']) !!}经济&nbsp;&nbsp;
                        {!! Form::radio('car_type_id','com',null,['id'=>'ct_com']) !!}舒适&nbsp;&nbsp;
                        {!! Form::radio('car_type_id','bus_7',null,['id'=>'ct_bus_7']) !!}商务7座&nbsp;&nbsp;
                        {!! Form::radio('car_type_id','suv',null,['id'=>'ct_suv']) !!}SUV&nbsp;&nbsp;
                        {!! Form::radio('car_type_id','lux',null,['id'=>'ct_lux']) !!}豪华&nbsp;&nbsp;
                        {!! Form::radio('car_type_id','cmb_14_55',null,['id'=>'cmb_14_55']) !!}14-55座中巴&nbsp;&nbsp;
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('flight_no','航班号:') !!}
                </div>
                <div class="field">
                    {!! Form::text('flight_no',null,['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('note','备注:') !!}
                </div>
                <div class="field">
                    {!! Form::textarea('note',null,['class'=>'input']) !!}
                </div>
            </div>

            {!! Form::hidden('order_type_id', 'airport') !!}
            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>