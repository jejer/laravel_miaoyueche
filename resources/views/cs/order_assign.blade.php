{? $page_name = "订单配车" ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('head')
<!--租车公司的变化-->
<script type="text/javascript">
    $(document).ready(function () {

        $("#car_company_id").change(function () {
            var car_company_id = $(this).val();
            var url = "{{ url('/cs/car_company_resources/0') }}";
            url = url.substring(0, url.length - 1) + car_company_id;

            $("#car_lpn").empty();

            $.ajax({
                url: url,
                type: 'GET'
            }).done(function(data) {
                $.each(data, function(lpn, model) {
                    $("#car_lpn").append('<option value="'+lpn+'">'+lpn+' '+model+'</option>')
                })
            });
        })
        $("#car_company_id").val($("#car_company_id option:first").val()).change();
    })
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($order, ['class'=>'form-x']) !!}

            @include('include.order_review')

            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_company_id','租车公司:') !!}
                </div>
                <div class="field">
                    {!! Form::select('car_company_id',$car_companies,null,['class'=>'input']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_lpn','车辆:') !!}
                </div>
                <div class="field">
                    {!! Form::select('car_lpn',[],null,['class'=>'input']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('driver_id','司机:') !!}
                </div>
                <div class="field">
                    {!! Form::select('driver_id',$drivers,null,['class'=>'input']) !!}
                </div>
            </div>
            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
@endsection