{? $page_name = "编辑司机" ?}
{? $parent_page = "drivers" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>

@include('include.head_fineuploader')

<script>
$(document).ready(function() {
    $('#fine-uploader-gallery-pic_id_1').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_id_1').val(responseJSON.uploadName);
            }
        }
    });
    $('#fine-uploader-gallery-pic_id_2').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_id_2').val(responseJSON.uploadName);
            }
        }
    });
    $('#fine-uploader-gallery-pic_license').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_license').val(responseJSON.uploadName);
            }
        }
    });
});
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($driver = $user->driver, ['method'=>'patch','url'=>'/cs/drivers/'.$user->id,'class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('email','登录邮箱:') !!}
                </div>
                <div class="field">
                    {!! Form::text('email',$user->email,['class'=>'input','data-validate'=>'required:登录邮箱','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('password','登录密码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('password',null,['class'=>'input', 'placeholder'=>'需要修改密码时才需要填写']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('username','用户名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('username',$user->name,['class'=>'input','data-validate'=>'required:用户名']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('wx_openid','微信ID:') !!}
                </div>
                <div class="field">
                    {!! Form::text('wx_openid',$user->wx_openid,['class'=>'input', 'disabled'=>True]) !!}
                </div>
            </div>

            @include('include.form_driver_info')

            <div class="form-button">
                {!! Form::submit('修改',['class'=>'button bg-main']) !!}
                <a class="button bg-main" href="{{ url('/cs/drivers', $user->id).'/delete' }}">删除</a>
            </div>
        {!! Form::close() !!}

    </div>
</div>
@endsection