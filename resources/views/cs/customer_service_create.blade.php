{? $page_name = "新建客服" ?}
{? $parent_page = "customer_services" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::open(['url'=>'/cs/customer_services','class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('email','登录邮箱:') !!}
                </div>
                <div class="field">
                    {!! Form::text('email',null,['class'=>'input','data-validate'=>'required:登录邮箱']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('password','登录密码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('password',null,['class'=>'input','data-validate'=>'required:登录密码']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('username','用户名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('username',null,['class'=>'input','data-validate'=>'required:用户名']) !!}
                </div>
            </div>

            @include('include.form_customer_service_info')
            
            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection