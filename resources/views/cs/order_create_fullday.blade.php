{? $page_name = '代客['.$customer->name.']下单 - 全日租' ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // <!--自驾代驾的变化-->
        $("#zijia").click(function () {
            $('#cmb_14_55').hide();
            $('#cmb_14_55').next('span').text('');

            $("#ct_eco").prop("checked", true).trigger("change");
        })
        $("#daijia").click(function () {
            $('#cmb_14_55').show();
            $('#cmb_14_55').next('span').text('14-55座中巴');

            $("#ct_eco").prop("checked", true).trigger("change");
        })
        // <!--价格的变化-->
        $("[name=car_type_id]").change(function() {
            switch (this.value) {
                case "eco": //jingji
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('150/天');
                } else {
                    $('#price').text('350/天');
                }
                break;

                case "com": //shushi
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('260/天');
                } else {
                    $('#price').text('460/天');
                }
                break;

                case "bus_7": //shangwu
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('280/天');
                } else {
                    $('#price').text('480/天');
                }
                break;

                case "suv": //suv
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('300/天');
                } else {
                    $('#price').text('500/天');
                }
                break;

                case "lux": //shehua
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('400/天');
                } else {
                    $('#price').text('600/天');
                }
                break;

                case "cmb_14_55": //zhongba
                if ($("[name=is_self][value=1]").is(':checked')) {
                    $('#price').text('不支持自驾中巴车型');
                } else {
                    $('#price').text('850/天');
                }
                break;
            }
        })
    
        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
        });

    })
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        <div class="alert alert-red whitea"><span class="close"></span><strong>注意：</strong>一些注意的信息，解释条款</div>
        {!! Form::model($order=new \App\Order, ['class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('is_self','自驾/代驾:') !!}
                </div>
                <div class="field">
                    <div class="button-group button-group-small radio">
                        <label id="zijia" class="button">
                            {!! Form::radio('is_self','1',null) !!}<span class="icon icon-check"></span>自驾
                        </label>
                        <label id="daijia" class="button active">
                            {!! Form::radio('is_self','0',null) !!}<span class="icon icon-check"></span>代驾
                        </label>
                    </div>
                </div>
            </div>

            @include('include.form_order_dayrent')

            <div class="form-group">
                <div class="label">
                    <label for="readme">价格</label>
                </div>
                <div class="field">
                    <span id="price" class="text-red " style="font-size: 25px;">350/天</span>
                </div>
            </div>

            {!! Form::hidden('order_type_id', 'fullday') !!}
            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection