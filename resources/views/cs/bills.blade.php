{? $parent_page = "bills" ?}

@extends('cs.master')

@section('head')
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        function process_bills(data) {
            console.log(data.last_page);
            // return;
            // 

            // $('.pagination').off('page');

            $('.pagination').bootpag({
                total: data.last_page,
                page: data.current_page,
                maxVisible: 10
            }).on('page', function(event, num){
                var url = data.next_page_url;
                if (!data.next_page_url) {
                    url = data.prev_page_url;
                }
                url = url.substring(0, url.indexOf("page=")) + "page=" + num;
                //console.log(url);
                $.ajax({
                    url: url,
                }).done(function(data) {
                    process_bills(data);

                    // 防止递归多次加载.. 我也不太明白为何
                    $('.pagination').off('page');
                });
            });


            $("tr.bills").remove();
            var page_total_price = 0;
            $.each(data.data, function(index, value) {
                page_total_price += value.price_final;
                if (value.order_type_id == 'airport') value.order_type_id = "接送机";
                if (value.order_type_id == 'halfday') value.order_type_id = "半日租代驾";
                if (value.order_type_id == 'fullday') value.order_type_id = "全日租代驾";
                if (value.order_type_id == 'fullday_self') value.order_type_id = "全日租自驾";
                $("table").append("<tr class='bills'>" + 
                    "<td>"+value.id+"</td>" + 
                    "<td>"+value.order_type_id+"</td>" + 
                    "<td>"+value.time_final+"</td>" + 
                    "<td>"+value.price_final+"</td>" + 
                    '<td><a class="button border-blue button-little" href="/cs/orders/'+value.id+'">查看</a></td>' + 
                    "</tr>");
            });
            $('#page_total_price').text(page_total_price);
        }

        $("#bill_query").click(function() {
            $.ajax({
                url: "/cs/bills/{{$type}}/{{$id}}/"+$("#start").val()+"/"+$("#end").val(),
            }).done(function(data) {
                process_bills(data);
                return;
            });
        });


        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
        });

    });
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="form-group">
        <div class="label">
            <label for="siteurl">起始日期</label>
        </div>
        <div class="field">
            <input id="start" type="text" class="input" data-field="date" data-validate="required:起始日期" readonly>
            <div id="dtBox"></div>
        </div>
    </div>
    <div class="form-group">
        <div class="label">
            <label for="siteurl">结束日期</label>
        </div>
        <div class="field">
            <input id="end" type="text" class="input" data-field="date" data-validate="required:结束日期" readonly>
            <div id="dtBox"></div>
        </div>
    </div>
    <a id="bill_query" class="button border-blue button-little" href="#">查询</a>

    <table class="table table-hover">
        <tr>
            <th width="120">订单号</th>
            <th width="120">分类</th>
            <th width="100">订单完成时间</th>
            <th width="60">金额</th>
            <th width="100">操作</th>
        </tr>

    </table>
    本页订单总金额 <div id='page_total_price'></div><br>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
<input type="button" onClick="window.print()" value="打印"/>
@endsection