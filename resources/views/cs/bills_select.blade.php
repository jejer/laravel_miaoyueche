{? $page_name = "对账单类型选择" ?}
{? $parent_page = "bills" ?}

@extends('cs.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::open(['class'=>'form-x']) !!}

            <div class="form-group">
                <div class="label">
                    {!! Form::label('type','类型:') !!}
                </div>
                <div class="field">
                    {!! Form::select('type',['all'=>'所有订单', 'by_customer'=>'按客户查询', 'by_car_company'=>'按租车公司查询'],null,['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('customer_id','客户公司:') !!}
                </div>
                <div class="field">
                    {!! Form::select('customer_id',$customers,null,['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('car_company_id','租车公司:') !!}
                </div>
                <div class="field">
                    {!! Form::select('car_company_id',$car_companies,null,['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
@endsection