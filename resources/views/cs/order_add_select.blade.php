{? $page_name = "代客下单" ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::open(['class'=>'form-x']) !!}

            <div class="form-group">
                <div class="label">
                    {!! Form::label('customer_id','客户公司:') !!}
                </div>
                <div class="field">
                    {!! Form::select('customer_id', $customers, null, ['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    {!! Form::label('order_type','订单类型:') !!}
                </div>
                <div class="field">
                    {!! Form::select('order_type', ['airport'=>'接送机','halfday'=>'半日租','fullday'=>'全日租'], null, ['class'=>'input']) !!}
                </div>
            </div>

            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection