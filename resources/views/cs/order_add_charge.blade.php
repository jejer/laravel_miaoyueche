{? $page_name = "新增订单附加收费" ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::open(['class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('name','收费项目') !!}
                </div>
                <div class="field">
                    {!! Form::text('name',null,['class'=>'input','data-validate'=>'required:收费项目']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('price','价格') !!}
                </div>
                <div class="field">
                    {!! Form::input('number','price',null,['class'=>'input','step'=>'0.01','data-validate'=>'required:价格']) !!}
                </div>
            </div>

            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection