{? $page_name = "订单详情" ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($order, ['class'=>'form-x']) !!}

            @include('include.order_review')

            <div class="form-button">
                @foreach ($buttons as $text =>$url)
                <a href='{{ $url }}' class="button button-small border-green">{{ $text }}</a>
                @endforeach
            </div>
        {!! Form::close() !!}
    </div>
</div>

{? $review_child_url = '/cs/orders' ?}
@include('include.order_review_child_orders')

@include('include.order_review_histories')

@include('include.order_review_charges')

<input type="button" onClick="window.print()" value="打印"/>

@endsection