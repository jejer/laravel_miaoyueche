{? $parent_page = "cars" ?}

@extends('cs.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination').bootpag({
            total: {{ $cars->lastPage() }},
            page: {{ $cars->currentPage() }},
            maxVisible: 10
        }).on('page', function(event, num){
            var url = "{{ $cars->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
            window.location.href = url;
        });
    })
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="padding border-bottom">
    <a class="button button-small border-green" href="{{ url('/cs/cars/create') }}">
        添加车辆
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/cars') }}">
        所有车辆
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/cars/deleted') }}">
        回收站
    </a>
    </div>
    <table class="table table-hover">
        <tr>
            <th width="120">车辆ID</th>
            <th width="120">所属公司</th>
            <th width="120">车牌号</th>
            <th width="120">车型</th>
            <th width="100">操作</th>
        </tr>
        @foreach ($cars as $car)
        <tr>
            <td>{{ $car->id }}</td>
            <td>{{ $car->car_company->name }}</td>
            <td>{{ $car->lpn }}</td>
            <td>{{ $car->model }}</td>
            <td><a class="button border-blue button-little" href="{{ url('/cs/cars', $car->id).'/edit' }}">查看/编辑</a></td>
        </tr>
        @endforeach
    </table>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
@endsection