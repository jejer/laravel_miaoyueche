{? $page_name = "编辑租车公司" ?}
{? $parent_page = "car_companies" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($user->car_company, ['method'=>'patch','url'=>'/cs/car_companies/'.$user->id,'class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('email','登录邮箱:') !!}
                </div>
                <div class="field">
                    {!! Form::text('email',$user->email,['class'=>'input','data-validate'=>'required:登录邮箱','disabled'=>True]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('password','登录密码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('password',null,['class'=>'input', 'placeholder'=>'需要修改密码时才需要填写']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('username','用户名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('username',$user->name,['class'=>'input','data-validate'=>'required:用户名']) !!}
                </div>
            </div>

            @include('include.form_car_company_info')
            
            <div class="form-button">
                {!! Form::submit('修改',['class'=>'button bg-main']) !!}
                <a class="button bg-main" href="{{ url('/cs/car_companies', $user->id).'/delete' }}">删除</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection