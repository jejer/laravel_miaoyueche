{? $page_name = "操作日志" ?}
{? $parent_page = "logs" ?}

@extends('cs.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination').bootpag({
            total: {{ $logs->lastPage() }},
            page: {{ $logs->currentPage() }},
            maxVisible: 10
        }).on('page', function(event, num){
            var url = "{{ $logs->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
            window.location.href = url;
        });
    })
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <table class="table table-hover">
        <tr>
            <th width="120">发生时间</th>
            <th width="120">用户类型</th>
            <th width="120">用户名</th>
            <th width="100">操作</th>
        </tr>
        @foreach ($logs as $log)
        <tr>
            <td>{{ $log->created_at }}</td>
            <td>{{ $log->user->role() }}</td>
            <td>{{ $log->user->name }}</td>
            <td>{{ $log->detail }}</td>
        </tr>
        @endforeach
    </table>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
@endsection