{? $page_name = "车辆信息" ?}
{? $parent_page = "cars" ?}


@extends('cs.master');


@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($car, ['class'=>'form-x']) !!}

            @include('include.order_review_car_info')

        {!! Form::close() !!}
    </div>
</div>
@endsection