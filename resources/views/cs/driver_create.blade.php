{? $page_name = "添加司机" ?}
{? $parent_page = "drivers" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>

@include('include.head_fineuploader')

<script>
$(document).ready(function() {
    $('#fine-uploader-gallery-pic_id_1').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_id_1').val(responseJSON.uploadName);
            }
        }
    });
    $('#fine-uploader-gallery-pic_id_2').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_id_2').val(responseJSON.uploadName);
            }
        }
    });
    $('#fine-uploader-gallery-pic_license').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/drivers',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_license').val(responseJSON.uploadName);
            }
        }
    });
});
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($driver = new \App\Driver, ['url'=>'/cs/drivers','class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('email','登录邮箱:') !!}
                </div>
                <div class="field">
                    {!! Form::text('email',null,['class'=>'input','data-validate'=>'required:登录邮箱']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('password','登录密码:') !!}
                </div>
                <div class="field">
                    {!! Form::text('password',null,['class'=>'input','data-validate'=>'required:登录密码']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    {!! Form::label('username','用户名:') !!}
                </div>
                <div class="field">
                    {!! Form::text('username',null,['class'=>'input','data-validate'=>'required:用户名']) !!}
                </div>
            </div>

            @include('include.form_driver_info')

            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
            </div>
        {!! Form::close() !!}

    </div>
</div>
@endsection