{? $page_name = '代客['.$customer->name.']下单 - 接送机' ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        // <!--价格的变化-->
        $("[name=car_type_id]").change(function() {
            switch (this.value) {
                case "eco": //jingji
                $('#price').text('160');
                break;

                case "com": //shushi
                $('#price').text('180');
                break;

                case "bus_7": //shangwu
                $('#price').text('200');
                break;

                case "suv": //suv
                $('#price').text('200');
                break;

                case "lux": //shehua
                $('#price').text('320');
                break;

                case "cmb_14_55": //zhongba
                $('#price').text('360');
                break;
            }
        })
    
        //时间日期选择器
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss",
            dateFormat: "yyyy-MM-dd",
            shortDayNames: ["周日","周一","周二","周三","周四","周五","周六"],
            fullDayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
            shortMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            fullMonthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
            titleContentDate: "选择日期",
            titleContentTime: "选择时间",
            titleContentDateTime: "选择日期和时间",
            setButtonContent: "设置",
            clearButtonContent: "清除"
        });

    })
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        <div class="alert alert-red whitea"><span class="close"></span><strong>注意：</strong>一些注意的信息，解释条款</div>
        {!! Form::model($order=new \App\Order, ['class'=>'form-x']) !!}

            @include('include.form_2airport')
            
            <div class="form-group">
                <div class="label">
                    <label for="readme">价格</label>
                </div>
                <div class="field">
                    <span id="price" class="text-red " style="font-size: 25px;">160</span>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection