{? $page_name = "编辑车辆" ?}
{? $parent_page = "cars" ?}

@extends('cs.master')

@section('head')
<script src="/js/pintuer.js"></script>

@include('include.head_fineuploader')

<script>
$(document).ready(function() {
    $('#fine-uploader-gallery-pic_license').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/cars',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_license').val(responseJSON.uploadName);
            }
        }
    });

    $('#fine-uploader-gallery-pic_insurance').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: '/upload/cars',
            customHeaders: {'X-CSRF-TOKEN': $("[name=_token]").val()}
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/images/placeholders/waiting-generic.png',
                notAvailablePath: '/images/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        },
        callbacks: {
            onComplete: function(id, name, responseJSON, xhr) {
                console.log(name);
                console.log(responseJSON);
                console.log(responseJSON.uploadName);
                $('input#pic_insurance').val(responseJSON.uploadName);
            }
        }
    });
});
</script>
@endsection

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::model($car, ['method'=>'patch','url'=>'/cs/cars/'.$car->id,'class'=>'form-x']) !!}

            @include('include.form_car_info')
            
            <div class="form-button">
                {!! Form::submit('提交',['class'=>'button bg-main']) !!}
                <a class="button bg-main" href="{{ url('/cs/cars', $car->id).'/delete' }}">删除</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection