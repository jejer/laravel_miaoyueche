{? $parent_page = "car_companies" ?}

@extends('cs.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination').bootpag({
            total: {{ $car_companies->lastPage() }},
            page: {{ $car_companies->currentPage() }},
            maxVisible: 10
        }).on('page', function(event, num){
            var url = "{{ $car_companies->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
            window.location.href = url;
        });
    })
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="padding border-bottom">
    <a class="button button-small border-green" href="{{ url('/cs/car_companies/create') }}">
        添加租车公司
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/car_companies') }}">
        所有租车公司
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/car_companies/deleted') }}">
        回收站
    </a>
    </div>
    <table class="table table-hover">
        <tr>
            <th width="120">租车公司ID</th>
            <th width="120">公司名称</th>
            <th width="100">操作</th>
        </tr>
        @foreach ($car_companies as $car_company)
        <tr>
            <td>{{ $car_company->id }}</td>
            <td>{{ $car_company->name }}</td>
            <td><a class="button border-blue button-little" href="{{ url('/cs/car_companies', $car_company->id).'/edit' }}">查看/编辑</a></td>
        </tr>
        @endforeach
    </table>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
@endsection