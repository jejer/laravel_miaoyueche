@extends('master')

@section('body')
<body>
    <div class="righter nav-navicon" id="admin-nav">
        <div class="mainer">
            <div class="admin-navbar">
                <span class="float-right">
                    <a class="button button-little bg-yellow" href="{{ url('/auth/logout') }}">注销登录</a>
                </span>
                <ul class="nav nav-inline admin-nav">
                    <li> <img  src="/images/logo.png" class="logo"/></li>
                    <li @if($parent_page == 'orders')class="active"@endif><a href="{{ url('/cs/orders') }}" class="icon-home right5">订单管理</a>
                </li>
            </li>
            <li @if($parent_page == 'customers')class="active"@endif><a href="{{ url('/cs/customers') }}" class="icon-user right5">客户管理</a></li>
            <li @if($parent_page == 'car_companies')class="active"@endif><a href="{{ url('/cs/car_companies') }}" class="icon-user right5">租车公司管理</a></li>
            <li @if($parent_page == 'cars')class="active"@endif><a href="{{ url('/cs/cars') }}" class="icon-user right5">车辆管理</a></li>
            <li @if($parent_page == 'drivers')class="active"@endif><a href="{{ url('/cs/drivers') }}" class="icon-user right5">司机管理</a></li>
            <li @if($parent_page == 'customer_services')class="active"@endif><a href="{{ url('/cs/customer_services') }}" class="icon-user right5">平台客服管理</a></li>
            <li @if($parent_page == 'logs')class="active"@endif><a href="{{ url('/cs/logs') }}" class="icon-user right5">日志</a></li>
            <li @if($parent_page == 'bills')class="active"@endif><a href="{{ url('/cs/bills_select') }}" class="icon-user right5">对账单</a></li>
        </ul>
    </div>
    <div class="admin-bread">
        <ul class="bread">
            <li><a href="#" class="icon-home">开始</a></li>
            <li>{{ $page_name or '' }}</li>
        </ul>
    </div>
</div>
</div>
<div class="admin">
<div class="line-big">

    @include('cs.sidebar')

    <div class="xm10">
        @if(Session::has('flash_message'))
        <div class="alert alert-blue whitea"><span class="close"></span>{{ Session::get('flash_message') }}</div>
        @endif

        @yield('main')
    </div>
</body>
@endsection

