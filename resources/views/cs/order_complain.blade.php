{? $page_name = "标记/取消投诉" ?}
{? $parent_page = "orders" ?}

@extends('cs.master')

@section('main')
<div class="panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="panel-body">
        {!! Form::open(['class'=>'form-x']) !!}
            <div class="form-group">
                <div class="label">
                    {!! Form::label('reason','原因') !!}
                </div>
                <div class="field">
                    {!! Form::text('reason',null,['class'=>'input','data-validate'=>'required:原因']) !!}
                </div>
            </div>

            <div class="form-button">
                @if ($order->getOriginal('is_complain'))
                {!! Form::submit('取消投诉',['class'=>'button bg-main']) !!}
                @else
                {!! Form::submit('标记投诉',['class'=>'button bg-main']) !!}
                @endif
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection