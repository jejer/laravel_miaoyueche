{? $parent_page = "drivers" ?}

@extends('cs.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination').bootpag({
            total: {{ $drivers->lastPage() }},
            page: {{ $drivers->currentPage() }},
            maxVisible: 10
        }).on('page', function(event, num){
            var url = "{{ $drivers->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
            window.location.href = url;
        });
    })
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="padding border-bottom">
    <a class="button button-small border-green" href="{{ url('/cs/drivers/create') }}">
        添加司机
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/drivers') }}">
        所有司机
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/drivers/deleted') }}">
        回收站
    </a>
    </div>
    <table class="table table-hover">
        <tr>
            <th width="120">司机ID</th>
            <th width="120">姓名</th>
            <th width="120">身份证号码</th>
            <th width="100">操作</th>
        </tr>
        @foreach ($drivers as $driver)
        <tr>
            <td>{{ $driver->id }}</td>
            <td>{{ $driver->name }}</td>
            <td>{{ $driver->id_number }}</td>
            <td><a class="button border-blue button-little" href="{{ url('/cs/drivers', $driver->id).'/edit' }}">查看/编辑</a></td>
        </tr>
        @endforeach
    </table>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
@endsection