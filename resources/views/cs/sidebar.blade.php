    <div class="xm2">
        <div class="panel border-back">
            <div class="panel-body text-center ">
                {{ $contact }}
            </div>
            <div class="panel-foot bg-back border-back">您好.</div>
        </div>
        <br />
        <div class="panel">
            <div class="panel-head"><strong>站点统计</strong></div>
            <ul class="list-group">
                <li><a href="{{ url('/cs/orders/state', 'waiting') }}"><span class="float-right badge bg-main">{{$order_count_waiting}}</span><span class="icon-shopping-cart"></span> 等待配车订单</a></li>
                <li><a href="{{ url('/cs/orders/state', 'inprogress') }}"><span class="float-right badge bg-main">{{$order_count_inprogress}}</span><span class="icon-shopping-cart"></span> 未完成订单</a></li>
                <li><a href="{{ url('/cs/orders/state', 'completed') }}"><span class="float-right badge bg-main">{{$order_count_completed}}</span><span class="icon-shopping-cart"></span> 已完成订单</a></li>
                <li><a href="{{ url('/cs/orders/state', 'all') }}"><span class="float-right badge bg-main">{{$order_count_all}}</span><span class="icon-shopping-cart"></span> 所有订单</a></li>
                <li><a href="{{ url('/cs/orders/state', 'deleted') }}"><span class="float-right badge bg-main">{{$order_count_deleted}}</span><span class="icon-shopping-cart"></span> 被删除订单</a></li>
            </ul>
        </div>
        <br />
    </div>