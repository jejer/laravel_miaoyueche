{? $parent_page = "customers" ?}

@extends('cs.master')

@section('head')
{{-- 分页 --}}
<script src="/js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination').bootpag({
            total: {{ $customers->lastPage() }},
            page: {{ $customers->currentPage() }},
            maxVisible: 10
        }).on('page', function(event, num){
            var url = "{{ $customers->url(0) }}";
            url = url.substring(0, url.length - 1) + num;
            window.location.href = url;
        });
    })
</script>
@endsection

@section('main')
<div class="panel admin-panel">
    <div class="panel-head"><strong>{{ $page_name }}</strong></div>
    <div class="padding border-bottom">
    <a class="button button-small border-green" href="{{ url('/cs/customers/create') }}">
        添加客户
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/customers') }}">
        所有客户
    </a>
    <a class="button button-small border-green" href="{{ url('/cs/customers/deleted') }}">
        回收站
    </a>
    </div>
    <table class="table table-hover">
        <tr>
            <th width="120">客户ID</th>
            <th width="120">公司</th>
            <th width="100">操作</th>
        </tr>
        @foreach ($customers as $customer)
        <tr>
            <td>{{ $customer->id }}</td>
            <td>{{ $customer->name }}</td>
            <td><a class="button border-blue button-little" href="{{ url('/cs/customers', $customer->id).'/edit' }}">查看/编辑</a></td>
        </tr>
        @endforeach
    </table>
    <div class="panel-foot text-center">
    <p class="pagination"></p>
    </div>
</div>
@endsection